-- MySQL dump 10.13  Distrib 8.0.18, for Win64 (x86_64)
--
-- Host: localhost    Database: humanitarna_baza
-- ------------------------------------------------------
-- Server version	8.0.18

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `blok`
--

DROP TABLE IF EXISTS `blok`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `blok` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `podrucje_blok` polygon NOT NULL,
  `naziv_blok` varchar(255) DEFAULT NULL,
  `status_blok` int(11) NOT NULL,
  `region_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKfq30crw7turvfphj4lmhmksyd` (`region_id`),
  CONSTRAINT `FKfq30crw7turvfphj4lmhmksyd` FOREIGN KEY (`region_id`) REFERENCES `regija` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blok`
--

LOCK TABLES `blok` WRITE;
/*!40000 ALTER TABLE `blok` DISABLE KEYS */;
INSERT INTO `blok` VALUES (1,_binary '\0\0\0\0\0\0\0\0\0\0\0\0\0�\���C3@�+�j��F@\�\�K�\'3@�\�\�kz\�F@-��d3@SX���\�F@\�\�2���3@�J\��F@\�\�e�Oo3@F�7�kG@�\���C3@�+�j��F@','prvi blok',1,2),(2,_binary '\0\0\0\0\0\0\0\0\0\0\0\0\0��~�\��2@�r�9>\�G@���\�+2@�׼���G@+�� \n2@8� \"5KG@��`P3@p�>;\�G@D\� �3@A��_�YG@��~�\��2@�r�9>\�G@','drugi',2,2),(3,_binary '\0\0\0\0\0\0\0\0\0\0\0\0\0\�1\0w/@	N} y_G@����O&1@�4E�ӇG@�\��\�1@%�\�1\�F@\�{c\0�0@���j\�F@\�Z|\n�\�.@a\�$\�/G@\�1\0w/@	N} y_G@','treci',3,2),(4,_binary '\0\0\0\0\0\0\0\0\0\0\0\0\0$c��}1@˟o�\�F@�2\�\�<1@\���StF@�\���?S2@y\�ZmF@�\��o^3@fffff\�F@$c��}1@˟o�\�F@','cetvrti',1,2);
/*!40000 ALTER TABLE `blok` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-01-16 23:42:19
