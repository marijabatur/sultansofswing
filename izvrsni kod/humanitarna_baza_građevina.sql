-- MySQL dump 10.13  Distrib 8.0.18, for Win64 (x86_64)
--
-- Host: localhost    Database: humanitarna_baza
-- ------------------------------------------------------
-- Server version	8.0.18

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `građevina`
--

DROP TABLE IF EXISTS `građevina`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `građevina` (
  `idgrađevina` bigint(20) NOT NULL AUTO_INCREMENT,
  `podrucje_građevina` geometry NOT NULL,
  `status_građevina` bit(1) DEFAULT NULL,
  `naziv_građevina` varchar(255) DEFAULT NULL,
  `block_id` bigint(20) NOT NULL,
  PRIMARY KEY (`idgrađevina`),
  UNIQUE KEY `UK_caqsp0jwj1fsq1i1rkvd7nqkh` (`naziv_građevina`),
  KEY `FK4pc5pbs1mrxavh95hi80opajd` (`block_id`),
  CONSTRAINT `FK4pc5pbs1mrxavh95hi80opajd` FOREIGN KEY (`block_id`) REFERENCES `blok` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `građevina`
--

LOCK TABLES `građevina` WRITE;
/*!40000 ALTER TABLE `građevina` DISABLE KEYS */;
INSERT INTO `građevina` VALUES (1,_binary '\0\0\0\0\0\0\0\0\0\0\0\0\0�!r�zb3@��w\��F@D\� E3@Л�T�F@\� ��J3@fO�s�F@U\�Yf3@�A\'��F@�!r�zb3@��w\��F@',_binary '','prva zgrada',1),(2,_binary '\0\0\0\0\0\0\0\0\0\0\0\0\0\�<t3@\�ZD�F@����\�i3@\�@1��F@J_9w3@��7h��F@З\��\\�3@�W˝��F@\�<t3@\�ZD�F@',_binary '\0','druga',1),(3,_binary '\0\0\0\0\0\0\0\0\0\0\0\0\0Mjh�i3@ePmp\"�F@�U]S3@؞Y�\�F@�T� b3@&��\�\�F@J�i�Wv3@\��f\�F@Mjh�i3@ePmp\"�F@',_binary '','treca',1),(4,_binary '\0\0\0\0\0\0\0\0\0\0\0\0\0Z\��c=3@^-w�F@�\��/3@I�p\�F@s��F3@\�~O�S\�F@�\Z�\'L3@\ZM.\���F@Z\��c=3@^-w�F@',_binary '\0','cetvrta',1),(5,_binary '\0\0\0\0\0\0\0\0\0\0\0\0\0\0u\�Y3@b�\Z���F@��\�|R3@[(��\��F@p\��/S3@&U\�M�\�F@�\�\��qk3@\�+�V]�F@\0u\�Y3@b�\Z���F@',_binary '','peta',1),(7,_binary '\0\0\0\0\0\0\0\0\0\0\0\0\0[\nH��1@	\���F@\0\0\0\0\0�1@���\�_�F@����?&2@VfJ\�o�F@b->�<2@	\���F@[\nH��1@	\���F@',_binary '','deseta zgrada',4),(8,_binary '\0\0\0\0\0\0\0\0\0\0\0\0\0[\nH�d2@\�uoEb�F@[\nH�d2@�~��F@$c��\�2@\�uoEb�F@[\nH�d2@\�uoEb�F@',_binary '','zgrada',4),(9,_binary '\0\0\0\0\0\0\0\0\0\0\0\0\0ܜJ��1@�xZ~\�F@����?r1@P5z5@�F@Mjh�2@��L�D�F@ܜJ��1@�xZ~\�F@',_binary '','aaaaaaaaaa',4),(10,_binary '\0\0\0\0\0\0\0\0\0\0\0\0\0\�<*�\�2@*\�dq��F@\�C\0p3@\�����F@�S\�\0Pa2@�Os�\"�F@\�<*�\�2@*\�dq��F@',_binary '','drugaaa',4);
/*!40000 ALTER TABLE `građevina` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-01-16 23:42:17
