-- MySQL dump 10.13  Distrib 8.0.18, for Win64 (x86_64)
--
-- Host: localhost    Database: humanitarna_baza
-- ------------------------------------------------------
-- Server version	8.0.18

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `korisnik`
--

DROP TABLE IF EXISTS `korisnik`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `korisnik` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `ime` varchar(255) NOT NULL,
  `prezime` varchar(255) NOT NULL,
  `lozinka` varchar(255) NOT NULL,
  `broj_mobitela` varchar(255) DEFAULT NULL,
  `slika` varchar(255) DEFAULT NULL,
  `registriran` bit(1) DEFAULT NULL,
  `uloga` varchar(255) NOT NULL,
  `korisnicko_ime` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_g77s6vv0ql6kiqdqk084f3fxi` (`korisnicko_ime`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `korisnik`
--

LOCK TABLES `korisnik` WRITE;
/*!40000 ALTER TABLE `korisnik` DISABLE KEYS */;
INSERT INTO `korisnik` VALUES (1,'sulejman@velicanstveni.com','Sulejman','Velicanstveni','{bcrypt}$2a$10$OBeoFlMDnMQtrZIXt1h13eP7eFjsOKPD0LWhYEYjWZKbNyb8kHiFa','000000000','https://storage.cloud.google.com/humanitarnokartografiranjeopp/sultan',_binary '','administrator','sultan'),(2,'ivo.ivic@leader.com','Ivo','Ivić','{bcrypt}$2a$10$KY.K9MiAWhl5osCF6x3vB.Ld3C.Bj2/7lADbuBNx6SVgYsaGPUwUi','11111111111','https://storage.cloud.google.com/humanitarnokartografiranjeopp/leader',_binary '','spasioc','leader'),(3,'ana.anic@cartographer.com','Ana','Anić','{bcrypt}$2a$10$CpJer6H1eHSB1utRR4NoSOuhVjOjPIwFwVct0CL6Y7b5hhydhm65e','000000000',NULL,_binary '','kartograf','cartographer'),(4,'pero.peric@rescuer.com','Pero','Perić','{bcrypt}$2a$10$eo8388DK0bNassiN609PKO.u9tuAfEDr99GENnbuUKITlC5nA6yOS','000000000','https://storage.cloud.google.com/humanitarnokartografiranjeopp/rescuer',_binary '','spasioc','rescuer');
/*!40000 ALTER TABLE `korisnik` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-01-16 23:42:18
