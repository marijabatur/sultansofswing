-- MySQL dump 10.13  Distrib 8.0.18, for Win64 (x86_64)
--
-- Host: localhost    Database: humanitarna_baza
-- ------------------------------------------------------
-- Server version	8.0.18

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `provjera_blok`
--

DROP TABLE IF EXISTS `provjera_blok`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `provjera_blok` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `provjerilo_kartografa` int(11) DEFAULT NULL,
  `block_id` bigint(20) NOT NULL,
  `cartographer_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKrkw4f95q8rkk2996as4dme96x` (`block_id`),
  KEY `FKkhk9vnmh7vr8nxapuc096dtw6` (`cartographer_id`),
  CONSTRAINT `FKkhk9vnmh7vr8nxapuc096dtw6` FOREIGN KEY (`cartographer_id`) REFERENCES `korisnik` (`id`),
  CONSTRAINT `FKrkw4f95q8rkk2996as4dme96x` FOREIGN KEY (`block_id`) REFERENCES `blok` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `provjera_blok`
--

LOCK TABLES `provjera_blok` WRITE;
/*!40000 ALTER TABLE `provjera_blok` DISABLE KEYS */;
INSERT INTO `provjera_blok` VALUES (16,0,4,3);
/*!40000 ALTER TABLE `provjera_blok` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-01-16 23:42:19
