import React from "react";
import {BrowserRouter, Switch, Route} from "react-router-dom";
import HeaderNeprijavljen from "./HeaderNeprijavljen";
import NaslovnaNeprijavljen from "./NaslovnaNeprijavljen";
import Prijava from "./Prijava";
import Registracija from "./Registracija";
import RegistracijaZaprimljena from "./RegistracijaZaprimljena";
import Potvrda from "./Potvrda";
import PocetnaAdministrator from "./PocetnaAdministrator"

function PocetnaNeprijavljen(props) {

  return (
    <BrowserRouter>
      <HeaderNeprijavljen/>
      <div className="PocetnaNeprijavljen">
        <Switch>
          <Route path="/" exact component={NaslovnaNeprijavljen}/>
          <Route path="/login" exact component={Prijava}/>
          <Route path="/registration" exact component={Registracija}/>
          <Route path="/zaprimljeno" exact component={RegistracijaZaprimljena}/>
          <Route path="/potvrda" exact component={Potvrda}/>
        </Switch>
      </div>
    </BrowserRouter>
  )

}

export default PocetnaNeprijavljen;
