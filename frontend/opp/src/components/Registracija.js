import React from "react";
import "./Formular.css";

function Registracija(props) {

  const [form, setForm] = React.useState({korisnickoIme: '', lozinka: '', fotografija: '', ime: '', prezime: '', brojMobitela: '', email: '', uloga: 'voditelj', registriran: false});
  const [fotografija, setFotografija] = React.useState('');
  const [error, setError] = React.useState('');

  async function onSubmit(e) {
    e.preventDefault();

    const user = {
        userName: form.korisnickoIme,
        password: form.lozinka,
        firstName: form.ime,
        lastName: form.prezime,
        email: form.email,
        phoneNumber: form.brojMobitela,
        role: form.uloga
    };

    const data = new FormData();
    data.append("picture", fotografija, `${form.korisnickoIme}`);
    
    const picoptions = {
      method: 'POST',
      body: data
    };

    fetch('/addPicture', picoptions);

    const options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(user)
    };

    const resp = await fetch('/registration', options);
    
    if(resp.status === 201){
      
      return props.history.push("/zaprimljeno");
    } else {
      const data = await resp.json();
      setError(data.message);
      
      }

  }

  function fileSelected(event){
    const file = event.target.files[0];
    console.log(file);

    setFotografija(file);
    onChange(event);
    
  }

  function onChange(event) {
    const {name, value} = event.target;
    setForm(oldForm => ({...oldForm, [name]: value}))
  }

  function isValid() {
    const {korisnickoIme, lozinka, ime, prezime, brojMobitela, email} = form;
    return korisnickoIme.length >= 4 && korisnickoIme.length <= 16 && lozinka.length >= 6 && lozinka.length <= 32 && ime.length > 0 && prezime.length > 0 && brojMobitela.length > 0 && email.length > 0;
  }

  return (
    <div className="Formular">
      <form onSubmit={onSubmit}>
        <div className="FormularRedak">
          <label>Korisničko ime:</label>
          <input name="korisnickoIme" onChange={onChange} value={form.korisnickoIme}/>
        </div>
        <div className="FormularRedak">
          <label>Lozinka:</label>
          <input name="lozinka" onChange={onChange} type="password" value={form.lozinka}/>
        </div>
        <div className="FormularRedak">
          <label>Fotografija:</label>
          <input name="fotografija" onChange={fileSelected} type="file" value={form.fotografija} accept="image/*"/>
        </div>
        <div className="FormularRedak">
          <label>Ime:</label>
          <input name="ime" onChange={onChange} value={form.ime}/>
        </div>
        <div className="FormularRedak">
          <label>Prezime:</label>
          <input name="prezime" onChange={onChange} value={form.prezime}/>
        </div>
        <div className="FormularRedak">
          <label>Broj mobitela:</label>
          <input name="brojMobitela" onChange={onChange} value={form.brojMobitela}/>
        </div>
        <div className="FormularRedak">
          <label>e-mail adresa:</label>
          <input name="email" onChange={onChange} value={form.email}/>
        </div>
        <div className="FormularRedak">
          <label>
            <input name="uloga" onChange={onChange} type="radio" value="voditelj" checked={form.uloga === "voditelj"}/>
            voditelj
          </label>
        </div>
        <div className="FormularRedak">
          <label>
            <input name="uloga" onChange={onChange} type="radio" value="spasioc" checked={form.uloga === "spasioc"}/>
            spasioc
          </label>
        </div>
        <div className="FormularRedak">
          <label>
            <input name="uloga" onChange={onChange} type="radio" value="kartograf" checked={form.uloga === "kartograf"}/>
            kartograf
          </label>
        </div>
        <div className='error'>{error}</div>
        <button type="submit" disabled={!isValid()}>Registracija</button>
      </form>
    </div>
  )

}

export default Registracija;