import React from "react";
import "./Poruka.css";

function RegistracijaZaprimljena() {

  return (
    <div className="Poruka">
      <p>Vaša registracija je zaprimljena!</p>
      <p>Pričekajte potvrdu administratora.</p>
    </div>
  )

}

export default RegistracijaZaprimljena;
