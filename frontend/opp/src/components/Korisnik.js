import React from "react";
import ReactDOM from "react-dom";
import "./Korisnik.css";

function Korisnik(props) {

  const {id, userName, firstName, lastName, phoneNumber, email, role} = props.korisnik;
  const [form, setForm] = React.useState({changed: false, userRole: props.korisnik.role});

  function onChange(event) {
    const {name, value} = event.target;
    setForm(oldForm => ({...oldForm, [name]: value, changed: true}));
  }

  function save() {
    
    const options = {
      method: 'POST',
      headers: {
       'Content-Type': 'application/x-www-form-urlencoded'
      },
      body: `username=${userName}&role=${form.userRole}`,
    };
    
    fetch('/admin/role', options);
    setForm(oldForm => ({...oldForm, changed: false}));
  }

  return (
    <div className="Korisnik">
      <label>{id} - {userName} - {firstName} {lastName} - {phoneNumber} - {email}</label>
      <form>
        <label>
          <input name="userRole" onChange={onChange} type="radio" value="administrator" checked={form.userRole === "administrator"}/>
          administrator
        </label>
        <label>
          <input name="userRole" onChange={onChange} type="radio" value="kartograf" checked={form.userRole === "kartograf"}/>
          kartograf
        </label>
        <label>
          <input name="userRole" onChange={onChange} type="radio" value="spasioc" checked={form.userRole === "spasioc"}/>
          spasioc
        </label>
        <label>
          <input name="userRole" onChange={onChange} type="radio" value="voditelj" checked={form.userRole === "voditelj"}/>
          voditelj
        </label>
        <button type="submit" onClick = {save} disabled={form.changed === false}>Spremi</button>
      </form>
    </div>
  );

}

export default Korisnik;
