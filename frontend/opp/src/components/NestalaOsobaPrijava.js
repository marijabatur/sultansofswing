import React from "react";
import "./Formular.css";

function NestalaOsobaPrijava(props) {

  const [form, setForm] = React.useState({ime: '', prezime: '', opis: '',fotografija: '', pronaden: false});
  const [fotografija, setFotografija] = React.useState('');
  const [error, setError] = React.useState('');

  async function onSubmit(e) {
    e.preventDefault();
    
    const missingPerson = {
      firstName: form.ime,
      lastName: form.prezime,
      description: form.opis,
  };

  const options = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(missingPerson)
  };

  const resp = await fetch('/missingPerson/report', options);
  
  if(resp.status === 200){

    const respData = await resp.json();
    console.log(respData);

    const data = new FormData();
    data.append("picture", fotografija, `${respData.id}`);
    
    const picoptions = {
      method: 'POST',
      body: data
    };
  
    fetch('/missingPerson/addPicture', picoptions);
  
    
    return props.history.push("/nestali");
  } else {
    const data = await resp.json();
    setError(data.message);
  }

 
  }

  function onChange(event) {
    const {name, value} = event.target;
    setForm(oldForm => ({...oldForm, [name]: value}))
  }

  
  function fileSelected(event){
    const file = event.target.files[0];

    setFotografija(file);
    onChange(event);
    
  }

  function isValid() {
    const {ime, prezime, opis} = form;
    return ime.length > 0 && prezime.length > 0 && opis.length > 20;
  }

  return (
  <div className="Formular">
    <form onSubmit={onSubmit}>
      <div className="FormularRedak">
        <label>Ime:</label>
        <input name="ime" onChange={onChange} value={form.ime}/>
        </div>
      <div className="FormularRedak">
        <label>Prezime:</label>
        <input name="prezime" onChange={onChange} value={form.prezime}/>
      </div>
      <div className="FormularRedak">
          <label>Opis:</label>
          <textarea name="opis" rows="10" cols="50" onChange={onChange} value={form.opis}/>
      </div>
      <div className="FormularRedak">
        <label>Fotografija:</label>
        <input name="fotografija"  onChange={fileSelected} type="file" value={form.fotografija} accept="image/*"/>
      </div>
      <div className='error'>{error}</div>
      <button type="submit" disabled={!isValid()}>Prijavi</button>
    </form>
  </div>
)

}

export default NestalaOsobaPrijava;
