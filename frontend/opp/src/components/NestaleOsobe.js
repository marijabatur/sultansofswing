import React from "react";
import NestalaOsoba from "./NestalaOsoba";
import "./Gumb.css";

function NestaleOsobe(props) {

   const [osobe, setOsobe] = React.useState([]);

  function prijavi() {
    return props.history.push("/prijavi");
  }

   React.useEffect(() => {
    fetch('/missingPerson/view')
      .then(data => data.json())
      .then(osobe => setOsobe(osobe))
    }, []);

  return (
    <div>
      <div className="Gumb">
        <button onClick={prijavi}>Prijavi nestalu osobu</button>
      </div>
      <div>
        {
          osobe.map(osoba =>
           <NestalaOsoba key={osoba.id} nestalaOsoba={osoba} uloga={props.uloga}/>
          )
        }
      </div>
    </div>
  )

}

export default NestaleOsobe;
