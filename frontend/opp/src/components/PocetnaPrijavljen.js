import React from "react";
import {BrowserRouter, Switch, Route} from "react-router-dom";
import HeaderPrijavljen from "./HeaderPrijavljen";
import Naslovna from "./NaslovnaPrijavljen"
import Odjava from "./Odjava"

function PocetnaPrijavljen(props){
    return(
        <BrowserRouter>
            <HeaderPrijavljen/>
            <div className="PocetnaPrijavljen">
                <Switch>
                    <Route path="/prijavljen" exact component={Naslovna}/>
                </Switch>
            </div>
        </BrowserRouter>
    )
}

export default PocetnaPrijavljen;