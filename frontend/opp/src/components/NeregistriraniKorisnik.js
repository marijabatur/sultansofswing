import React from "react";
import ReactDOM from "react-dom";
import "./NeregistriraniKorisnik.css";

function NeregistriraniKorisnik(props) {

  const {id, userName, password, fotografija, firstName, lastName, phoneNumber, email, role, registered} = props.neregistriraniKorisnik;
  const [hidden, setHidden] = React.useState(false);
  const [output, set] = React.useState('');

  function accept() {

    const options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(props.neregistriraniKorisnik)
    };

    fetch("/admin/view/accept", options).then(response => {
      if(response.status === 200){
        setHidden(true);
      } else {

      }
    });
  }

  function decline(){
    const options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(props.neregistriraniKorisnik)
    };

    fetch("/admin/view/decline", options);
    setHidden(true);
    return("Zahtjev za registraciju obrisan.");
  }

  return (
    <div className="NeregistriraniKorisnik">
      {hidden === false && <button type="submit" onClick = {accept}>Prihvati</button>}
      {hidden === false && <button type="submit" onClick = {decline}>Odbij</button>}
      {hidden === false && <label>{id} - {userName} - {firstName} {lastName} - {phoneNumber} - {email} - {role}</label>}
    </div>
  )

}

export default NeregistriraniKorisnik;
