import React from "react";
import Komentar from "./Komentar";
import "./Komentari.css"

function Komentari(props) {

   const [komentari, setKomentari] = React.useState([]);
   const [form, setForm] = React.useState({id: props.id, tekst: ''});

  function onChange(event) {
    const {name, value} = event.target;
    setForm(oldForm => ({...oldForm, [name]: value}))
  }

  function isValid() {
    return form.tekst.length > 10;
  }

  function dodaj() {

    const options = {
      method: 'POST',
      headers: {
       'Content-Type': 'application/x-www-form-urlencoded'
      },
      body: `id=${form.id}&comment=${form.tekst}`,
    };

    fetch("/missingPerson/comments/add", options);
  }

  React.useEffect(() => {
   fetch(`/missingPerson/comments?id=${form.id}`)
     .then(data => data.json())
     .then(komentari => setKomentari(komentari))
  }, []);

  return (
  <div>
    {
      komentari.map(komentar =>
        <Komentar key={komentar.id} komentar={komentar}/>
     )
    }
    <div className="Upis">
     <h3>Komentiraj</h3>
     <textarea name="tekst" onChange={onChange} value={form.tekst}/>
     <button type="submit" disabled ={!isValid()} onClick = {dodaj}>Dodaj komentar</button>
    </div>
  </div>
);

}

export default Komentari;
