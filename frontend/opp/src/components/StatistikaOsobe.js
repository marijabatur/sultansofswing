import React from "react";
import LineChart from "react-linechart";
import d3 from "d3";
import "./Statistika.css";

function StatistikaOsobe(props) {
  const [reportedPoints, setReportedPoints] = React.useState([]);
  const [foundPoints, setFoundPoints] = React.useState([]);

  const [reportedColor, setReportedColor] = React.useState("red");
  const [foundColor, setFoundColor] = React.useState("green");

  React.useEffect(() => {
      fetch(`missingPerson/statistics/reported`)
        .then(data => data.json())
        .then(reportedPoints => setReportedPoints(reportedPoints))
  }, []);

  React.useEffect(() => {
    fetch(`missingPerson/statistics/found`)
      .then(data => data.json())
      .then(foundPoints => setFoundPoints(foundPoints))
  }, []);

 const missingPersonStatistics = [
   {
     name: "broj prijavljenih osoba",
     color: reportedColor,
     points: reportedPoints,
   },
   {
     name: "broj pronađenih osoba",
     color: foundColor,
     points: foundPoints,
   }
 ];

  return (
    <div>
      <div className="Statistika">
        <h3>Broj prijavljenih nestalih i pronađenih osoba kroz vrijeme</h3>
        <div className="StatistikaTablica">
          <LineChart
            hideXAxis = {false}
            hideYAxis={false}
            height={400}
            width={1150}
            interpolate={"linear"}
            xDisplay={d3.time.format("%d.%m.%Y")}
            yMin={"0"}
            ticks={10}
            isDate={true}
            data={missingPersonStatistics}
            xLabel={"datum"}
            yLabel={"broj osoba"}
            legendPosition={"top-center"}
            showLegends={true}
          />
        </div>
      </div>
    </div>
 
  );

}

export default StatistikaOsobe;
