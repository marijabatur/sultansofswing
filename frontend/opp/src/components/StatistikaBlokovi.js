import React from "react";
import LineChart from "react-linechart";
import d3 from "d3";
import "./Statistika.css";

function StatistikaBlokovi(props) {
  const [finishedPoints, setFinishedPoints] = React.useState([]);

  React.useEffect(() => {
    fetch(`/block/statistics`)
      .then(data => data.json())
      .then(finishedPoints => setFinishedPoints(finishedPoints))
  }, []);

const blockStatistics = [
  {
    name: "broj završenih blokova",
    color:"blue",
    points: finishedPoints
  }
];

  return (
    <div>
      <div className="Statistika">
        <h3>Broj završenih blokova kroz vrijeme</h3>
        <div className="StatistikaTablica">
          <LineChart
            hideXAxis = {false}
            hideYAxis={false}
            height={400}
            width={1150}
            interpolate={"linear"}
            xDisplay={d3.time.format("%d.%m.%Y")}
            yMin={"0"}
            ticks={10}
            isDate={true}
            data={blockStatistics}
            xLabel={"datum"}
            yLabel={"broj blokova"}
            onPointHover= {(obj) => `<div className="StatistikaTablica"><label>datum: ${obj.x}<br/>broj završenih blokova: ${obj.y}</label></div>`}
            legendPosition={"top-center"}
            showLegends={true}
          />
        </div>
      </div>
    </div>
 
  );

}

export default StatistikaBlokovi;