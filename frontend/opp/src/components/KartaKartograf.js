import React, {Component} from "react";
import L from 'leaflet';
import 'leaflet/dist/leaflet.css';
import 'leaflet-draw';
import 'leaflet-draw/dist/leaflet.draw.css';
import styled from 'styled-components';
import 'geojson';

const Wrapper = styled.div`
  width: ${props => props.width};
   height: ${props => props.height};
`;

var GeoJSON = require('geojson');

export default class KartaKartograf extends Component {


  constructor(props) {
    super(props);
    this.state = {
      username: props.username,
      uloga:props.uloga,
      defineBuilding: false,
      areaDefined: false,
      naziv: '',
      nazivBlok: '',
      buildingError: '',
      chooseBlock: false,
      checksEnabled: false
    }
  }
  onMapClick(e) {
    var popup = L.popup();
    popup.setLatLng(e.latlng).setContent('You clicked the map' + e.latlng.toString()).openOn(e.target);
  }

  async onSubmitBuilding(e) {
    e.preventDefault();
    
    const building = {
      name: this.state.naziv,
      blockName: this.state.nazivBlok,
      area: this.state.layer.toGeoJSON().geometry.coordinates[0],
    };

    const options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(building)
    };

    const resp = await fetch('/building/add', options);

    if(resp.status === 200){
      this.setState({
        buildingError: ''
      })
    } else {
      const data = await resp.json();
      this.setState({
        buildingError: data.message
      })
    }
  }
  
  onClick(e) {
    this.setState({
      chosenBlock: e.target,
    });
    e.target.setStyle({color: 'black'});
  }

  onCheckClick(e) {
    this.setState({
      checkBlock: e.target,
    });
    e.target.setStyle({color: 'black'});
  }

  async componentDidMount() {
    this.map = L.map('map', {
      center: [46,16],
      zoom: 7,
      zoomControl: false,
    });

    L.tileLayer('https://api.maptiler.com/maps/streets/{z}/{x}/{y}.png?key=yCR2MJCLGXABQBsbaOR9', {
      attribution: '<a href="https://www.maptiler.com/copyright/" target="_blank">&copy; MapTiler</a> <a href="https://www.openstreetmap.org/copyright" target="_blank">&copy; OpenStreetMap contributors</a>',
      detectRetina: true,
      maxZoom: 50,
      maxNativeZoom: 17,
      drawControl: true,
    }).addTo(this.map);
      
    var center = [52.520, 13.405];
    L.marker(center).addTo(this.map);
  
    let resp = await fetch(`/cartographer/block/get?username=${this.state.username}`);
    if(resp.status === 200){
      const data = await resp.json();
      this.setState({
        polygon: data,
        nazivBlok: data.name
      })

    } else {
      const data = await resp.json();
      this.setState({
        error: data.message,
      });

      if(data.message === "You have no active blocks.") {
        this.setState({
          chooseBlock: true,
        });

        resp = await fetch(`/blocks/get/notStarted`);
        if(resp.status === 200){
          const data = await resp.json();
          this.setState({
            blocks: data
          })
          for (let index = 0; index < this.state.blocks.length; index++) {
            
            var geojsonFeature = {
              "type": "Feature",
              "properties": {
                  "name": this.state.blocks[index].name,
                  "amenity": "My block",
                  "popupContent": "This is where I map buildings!"
              },
              "geometry": {
                "type": "Polygon",
                "coordinates": [this.state.blocks[index].area]
              }
            };
            var layer = L.geoJSON(geojsonFeature, {"color":"red"});
            layer.addTo(this.map);
            layer.on('click', this.onClick.bind(this));
          }  
         console.log(this.state.blocks)
  
        } else {
        const data = await resp.json();
        this.setState({
          error: data.message
        })
        
        }
      }
    }

    if(this.state.polygon){
      var geojsonFeature = {
        "type": "Feature",
        "properties": {
            "name": "Block",
            "amenity": "My block",
            "popupContent": "This is where I map buildings!",
        },
        "geometry": {
          "type": "Polygon",
          "coordinates": [this.state.polygon.area]
        }
    };
      L.geoJSON(geojsonFeature, {"color": "blue", "name":`${this.state.polygon.name}`}).addTo(this.map);

      resp = await fetch(`/buildings/get?blockName=${this.state.polygon.name}`);
      if(resp.status === 200){
        const data = await resp.json();
        this.setState({
          buildings: data
        })
  
      } else {
        const data = await resp.json();
        this.setState({
          error: data.message
        })
        
      }
  
      if(this.state.buildings){
  
        for (let index = 0; index < this.state.buildings.length; index++) {
          
          var geojsonFeature = {
            "type": "Feature",
            "properties": {
                "name": this.state.buildings[index].name,
                "amenity": "My block",
                "popupContent": "This is where I map buildings!"
            },
            "geometry": {
              "type": "Polygon",
              "coordinates": [this.state.buildings[index].area]
            }
          };
          L.geoJSON(geojsonFeature, {"name":`${this.state.buildings[index].name}`}).addTo(this.map);
          
        }
      }
    }



    var editableLayers = new L.FeatureGroup();
    this.map.addLayer(editableLayers);
  
    var drawPluginOptions = {
      position: 'topright',
      draw: {
        polygon: {
          allowIntersection:false,
          drawError: {
            color: '#e1e100',
            message: 'You can\'t draw that!'
          },
          shapeOptions: {
            color: '#97009c'
          },
            
        },
  
        polyline: false,
        circle: false,
        rectangle: false,
        marker: false,
        circlemarker: false,
          
      },
      edit: {
        featureGroup: editableLayers,
        remove: false
      }
    };
  
    var drawControl = new L.Control.Draw(drawPluginOptions);
    this.map.addControl(drawControl);
    var geojson= new L.GeoJSON();

    this.map.on('draw:created', (e) => {
      let layer = e.layer;
       
      this.setState({
        layer: layer,
        areaDefined:true,
      });
  
      editableLayers.addLayer(layer);
    });  
      
  }
  
  odaberiBlok(e) {
    e.preventDefault();

    const options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      body: `username=${this.state.username}&blockName=${this.state.chosenBlock.options.name}`
    };

    this.setState({
      nazivBlok: this.state.chosenBlock.options.name 
    });

    this.state.chosenBlock.setStyle({color: 'blue'});

    fetch('/cartographer/block/set', options);
    this.props.history.push("/kartaKartograf");
  }

  changeToCheck(e) {
    e.preventDefault();
    
    const options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      body: `username=${this.state.username}&blockName=${this.state.nazivBlok}`
    };

    fetch('/cartographer/block/finish', options);
  }

  handleClick = () => {
    const map = this.mapRef.current
    if (map != null) {
      map.leafletElement.locate()
    }
  }

  async provjeriBlokove(e) {
    e.preventDefault();

    let resp = await fetch(`/blocks/get/check`);
    if(resp.status === 200){
      const data = await resp.json();
      this.setState({
        checkBlocks: data,
        checksEnabled: true
      })
      for (let index = 0; index < this.state.checkBlocks.length; index++) {
        
        var geojsonFeature = {
          "type": "Feature",
          "properties": {
              "name": this.state.checkBlocks[index].name,
              "amenity": "My block",
              "popupContent": "This is where I map buildings!"
          },
          "geometry": {
            "type": "Polygon",
            "coordinates": [this.state.checkBlocks[index].area]
          }
        };
        var layer = L.geoJSON(geojsonFeature, {
          "color":"yellow",
          "name": `${this.state.checkBlocks[index].name}`
        });
        layer.addTo(this.map);
        layer.on('click', this.onCheckClick.bind(this));
      }  

    } else {
      const data = await resp.json();
      this.setState({
        error: data.message
      })
        
    }
    
  }

  odobriBlok(e) {
    e.preventDefault();
    
    console.log(this.state.checkBlock.options.name);
    const options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      body: `username=${this.state.username}&blockName=${this.state.checkBlock.options.name}`
    };

    fetch('/cartographer/block/approve', options);

    this.state.checkBlock.setStyle({color: 'green'});
  }

  ispraviBlok(e) {
    e.preventDefault();

    const options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      body: `username=${this.state.username}&blockName=${this.state.checkBlock.options.name}`
    };

    fetch('/cartographer/block/disapprove', options);
    this.state.checkBlock.setStyle({color: 'red'});
  }

  render() {
    return(
      <div>
        <div className='error'>{this.state.error}</div>
        <Wrapper width= "1280px" height="720px" id="map" />

        <button onClick={this.changeToCheck.bind(this)}>Promijeni stanje bloka u provjeru</button>

        <button type="button" onClick={(e)=>{this.setState({
          defineBuilding: !this.state.defineBuilding,
          naziv: ''

        });}}>Dodaj zgradu</button>

        <button type="button" onClick={this.odaberiBlok.bind(this)}>Odaberi blok</button>
        <button type="button" onClick={this.provjeriBlokove.bind(this)}>Provjeri blokove</button>
        
        <div hidden={!this.state.checksEnabled}>
          <button type="button" onClick={this.odobriBlok.bind(this)}>Odobri blok</button>
          <button type="button" onClick={this.ispraviBlok.bind(this)}>Ispravi blok</button>
        </div>

        <div className="Formular" hidden={!this.state.defineBuilding}>
          <form onSubmit={this.onSubmitBuilding.bind(this)}>
            <div className="FormularRedak">
              <label>Naziv zgrade:</label>
              <input name="naziv" onChange={(e)=>{this.setState({
                naziv: e.target.value,
              });}} value={this.state.naziv}/>
            </div>
            <div className='error'>{this.state.buildingError}</div>
            <button type="submit" disabled={!this.state.areaDefined}>Spremi</button>
          </form>
        </div>
      </div>
    )
  }
}