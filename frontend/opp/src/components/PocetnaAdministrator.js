import React from "react";
import {BrowserRouter, Switch, Route} from "react-router-dom";
import HeaderAdministrator from "./HeaderAdministrator";
import NaslovnaAdministrator from "./NaslovnaAdministrator";
import Zahtjevi from "./Zahtjevi";
import Odjava from "./Odjava"

function PocetnaAdministrator(props) {

  return (
    <BrowserRouter>
      <HeaderAdministrator/>
      <div className="PocetnaAdministrator">
        <Switch>
          <Route path="/admin" exact component={NaslovnaAdministrator}/>
          <Route path="/admin/view" exact component={Zahtjevi}/>
          <Route path="/logout" exact component={Odjava}/>
        </Switch>
      </div>
    </BrowserRouter>
  )

}

export default PocetnaAdministrator;
