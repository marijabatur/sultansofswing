import React from "react";
import Korisnik from "./Korisnik";

function Korisnici() {

  const [korisnici, setKorisnici] = React.useState([]);
  
  React.useEffect(() => {
    fetch('/admin/users')
    .then(data => data.json())
    .then(korisnici => setKorisnici(korisnici))
  }, []);
  

  return (
    <div>
    {
      korisnici.map(korisnik =>
        <Korisnik key={korisnik.id} korisnik={korisnik}/>
      )
    }
    </div>
  );

}

export default Korisnici;
