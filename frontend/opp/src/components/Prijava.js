import React from "react";
import "./Formular.css";
import PocetnaAdministrator from "./PocetnaAdministrator"

function Prijava(props) {

  const [form, setForm] = React.useState({korisnickoIme: '', lozinka: ''});
  const [error, setError] = React.useState('');

 async function onSubmit(e) {
    e.preventDefault();


  const options = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'

    },
    body: `userName=${form.korisnickoIme}&password=${form.lozinka}`

  };

  const resp = await fetch('/login', options);
    
    if(resp.status === 200){
      const data = await resp.json();
      console.log(data)
      props.setRole(data.role, data.userName);
      props.history.push("/")
    } else {
      setError("Incorrect username or password!");
      setForm(oldForm => ({korisnickoIme: '', lozinka: ''}));
    }

  }

  function onChange(event) {
    const {name, value} = event.target;
    setForm(oldForm => ({...oldForm, [name]: value}))
  }

  function isValid() {
    const {korisnickoIme, lozinka} = form;
    return korisnickoIme.length >= 4 && korisnickoIme.length <= 16 && lozinka.length >= 6 && lozinka.length <= 32;
  }

  return (
    <div className="Formular">
      <form onSubmit={onSubmit}>
        <div className="FormularRedak">
          <label>Korisničko ime:</label>
          <input name="korisnickoIme" onChange={onChange} value={form.korisnickoIme}/>
        </div>
        <div className="FormularRedak">
          <label>Lozinka:</label>
          <input name="lozinka" onChange={onChange} type="password" value={form.lozinka}/>
        </div>
        <div className='error'>{error}</div>
        <button type="submit" disabled={!isValid()}>Prijava</button>
      </form>
    </div>
  )

}

export default Prijava;
