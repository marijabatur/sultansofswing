import React from "react";
import ReactDOM from "react-dom";
import "./Promjena.css";

function Promjena(props) {

  const [form, setForm] = React.useState({ lozinka: '', fotografija: '', ime: '', prezime: '', brojMobitela: '', email: ''});
  const [fotografija, setFotografija] = React.useState('');
  const [error, setError] = React.useState('');

  async function promijeniFotografiju(e) {
    e.preventDefault();

    const data = new FormData();
    data.append("picture", fotografija, `${props.username}`);
    
    const picoptions = {
      method: 'POST',
      body: data
    };

    const resp = await fetch('/addPicture', picoptions);

    if(resp.status === 200){
      
      return props.history.push("/profil");
    } else {
      const data = await resp.json();
      setError(data.message);
      
    }
  }

  async function promijeniPodatke(e) {
    e.preventDefault();

    const options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      body: `username=${props.username}&password=${form.lozinka}&firstName=${form.ime}&lastName=${form.prezime}&email=${form.email}&phoneNumber=${form.brojMobitela}`
    };

    const resp = await fetch('/user/change', options);

    if(resp.status === 200){
      
      return props.history.push("/profil");
    } else {
      const data = await resp.json();
      setError(data.message);
      
    }

  
  }

  function onChange(event) {
    const {name, value} = event.target;
    setForm(oldForm => ({...oldForm, [name]: value}))
  }

  function fileSelected(event) {

    const file = event.target.files[0];
    console.log(file);

    setFotografija(file);
    onChange(event);

  }

  return(
    <div>
      <div className="Promjena">
        <h3>Promjena fotografije</h3>
        <form onSubmit={promijeniFotografiju}>
          <label>Nova fotografija:</label>
          <input name="fotografija" value={form.fotografija} onChange={fileSelected} type="file" accept="image/*"/>
          <button type="submit">Spremi</button>
        </form>
      </div>
      
      <div className="Promjena">
        <h3>Promjena osobnih podataka</h3>
        <form onSubmit={promijeniPodatke}>
          <label>Lozinka:</label>
          <input name="lozinka" onChange={onChange} value={form.lozinka}/>
          <label>Ime:</label>
          <input name="ime" onChange={onChange} value={form.ime}/>
          <label>Prezime:</label>
          <input name="prezime" onChange={onChange} value={form.prezime}/>
          <label>Broj mobitela:</label>
          <input name="brojMobitela" onChange={onChange} value={form.brojMobitela}/>
          <label>e-mail adresa:</label>
          <input name="email" onChange={onChange} value={form.email}/>

          <div className='error'>{error}</div>

          <button type="submit">Spremi</button>
        </form>
      </div>
    </div>
  );

}

export default Promjena;
