import React from "react";
import ReactDOM from "react-dom";
import "./Redak.css";
import Komentari from "./Komentari";
import "./NestalaOsoba.css";


function NestalaOsoba(props) {

  const {id, firstName, lastName, pictureURL, description} = props.nestalaOsoba;
  const [showInfo, setShowInfo] = React.useState(false);
  const [hidden, setHidden] = React.useState(false);

  function pregledaj() {
    setShowInfo(!showInfo);
  }

  function zakljucaj() {

    const options = {
      method: 'POST',
      headers: {
       'Content-Type': 'application/x-www-form-urlencoded'
      },
      body: `id=${id}`,
    };

    fetch('/missingPerson/lock', options);
    setHidden(true);
  }

  function obrisi() {
    const options = {
      method: 'POST',
      headers: {
       'Content-Type': 'application/x-www-form-urlencoded'
      },
      body: `id=${id}`,
    };

    fetch('/missingPerson/leader/delete', options);
    setHidden(true);
  }

  return (
    <div className="Redak" hidden={hidden}>
      <button type="submit" onClick={pregledaj}>Pregledaj</button>
      {props.uloga && ((props.uloga === "spasioc") || (props.uloga === "voditelj") || (props.uloga === "administrator")) && <button type="submit" onClick = {zakljucaj}>Zaključaj</button>}
      {props.uloga && ((props.uloga === "voditelj") || (props.uloga === "administrator")) && <button type="submit" onClick = {obrisi}>Obriši</button>}


      <label>{firstName} {lastName}</label>

      <div hidden={!showInfo}>
        <img src={pictureURL}/>

        <h3>Opis:</h3>
        <label>{description}</label>
        <h3>Komentari</h3>
        <Komentari id={id}/>
      </div>
    </div>
  )

}

export default NestalaOsoba;
