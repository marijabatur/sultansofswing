import React from "react";
import "./Naslovna.css";

function Naslovna(props) {

  return (
    <div className="Naslovna">
      <h1>Humanitarno kartografiranje</h1>
      {props.uloga && <h2>Prijavljeni ste kao {props.uloga}.</h2>}
    </div>
  )

}

export default Naslovna;