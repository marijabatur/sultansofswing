import React from "react";
import "./Odjava.css";
import HeaderNeprijavljen from "./HeaderNeprijavljen"

class Odjava extends React.Component {
    
    constructor(props){
        super(props);
        props.setRole(null, null);
    }
   
    render(){
        return(
            <div className="Odjava">
                <h2>Uspješno ste odjavljeni.</h2>
            </div>
            
        );
    }
 
}

export default Odjava;