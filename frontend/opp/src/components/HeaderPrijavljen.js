import React from "react";
import {Link} from "react-router-dom";
import "./Header.css";

function HeaderPrijavljen(props) {

    return (
      <header className="Header">
        <Link to="/prijavljen">Naslovna</Link>
        <Link to="/logout">Odjava</Link>
      </header>
    )
  
  }
  
  export default HeaderPrijavljen;