import React from "react";
import {Link} from "react-router-dom";
import "./Header.css";

function Header(props) {

  return (
    <header className="Header">
      <Link to="/">Naslovna</Link>
      <Link to="/nestali">Nestale osobe</Link>
      {!props.uloga && <Link to="/login">Prijava</Link>}
      {!props.uloga && <Link to="/registracija">Registracija</Link>}
      {props.uloga && (props.uloga === "administrator") && <Link to="/korisnici">Korisnici</Link>}
      {props.uloga && (props.uloga === "administrator") && <Link to="/zahtjevi">Zahtjevi za registraciju</Link>}
      {props.uloga && (props.uloga === "administrator") && <Link to="/statistika">Statistika</Link>}
      {props.uloga && (props.uloga === "administrator" || props.uloga === "voditelj") && <Link to="/kartaVoditelj">Karta</Link>}
      {props.uloga && <Link to="/profil">Profil</Link>}
      {props.uloga && <Link to="/logout">Odjava</Link>}
      {props.uloga && (props.uloga === "kartograf") && <Link to="/kartaKartograf">Karta</Link>}
      {props.uloga && (props.uloga === "spasioc") && <Link to="/kartaSpasioc">Karta</Link>}
    </header>
  )

}

export default Header;
