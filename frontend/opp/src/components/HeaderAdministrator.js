import React from "react";
import {Link} from "react-router-dom";
import "./Header.css";

function HeaderAdministrator(props) {

  return (
    <header className="Header">
      <Link to="/administrator">Naslovna</Link>
      <Link to="/admin/view">Zahtjevi za registraciju</Link>
      <Link to="/logout">Odjava</Link>
    </header>
  )

}

export default HeaderAdministrator;
