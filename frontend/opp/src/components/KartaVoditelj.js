import React, {Component} from "react";
import L from 'leaflet';
import 'leaflet/dist/leaflet.css';
import 'leaflet-draw';
import 'leaflet-draw/dist/leaflet.draw.css';
import styled from 'styled-components';
import 'geojson';

const Wrapper = styled.div`
    width: ${props => props.width};
    height: ${props => props.height};
`;

export default class Karta extends Component {

  constructor(props){
    super(props);
    this.state={
      uloga: props.uloga,
      defineRegion: false,
      defineBlock: false,
      defineBuilding: false,
      areaDefined: false,
      naziv: '',
      nazivRegija: '',
      nazivBlok: '',
      blockError: '',
      buildingError: ''
    }
  }

  onMapClick(e) {
    var popup = L.popup();
    popup.setLatLng(e.latlng).setContent('You clicked the map' + e.latlng.toString()).openOn(e.target);
  }

  async componentDidMount(){
    this.map = L.map('map', {
      center: [46,16],
      zoom: 7,
      zoomControl: false,
    });

    L.tileLayer('https://api.maptiler.com/maps/streets/{z}/{x}/{y}.png?key=yCR2MJCLGXABQBsbaOR9', {
      attribution: '<a href="https://www.maptiler.com/copyright/" target="_blank">&copy; MapTiler</a> <a href="https://www.openstreetmap.org/copyright" target="_blank">&copy; OpenStreetMap contributors</a>',
      detectRetina: true,
      maxZoom: 50,
      maxNativeZoom: 17,
      drawControl: true,
    }).addTo(this.map);

    var center = [52.520, 13.405];
    L.marker(center).addTo(this.map);

    var editableLayers = new L.FeatureGroup();
    this.map.addLayer(editableLayers);

    var drawPluginOptions = {
      position: 'topright',
      draw: {
        polygon: {
          allowIntersection: false,
          drawError: {
            color: '#e1e100',
            message: '<strong>Oh snap!<strong> you can\'t draw that!'
          },
          shapeOptions: {
            color: '#97009c'
          }
        },

        polyline: false,
        circle: false,
        rectangle: false,
        marker: false,
        circlemarker: false,
      },
      edit: {
        featureGroup: editableLayers,
        remove: false
      }
    };

    var drawControl = new L.Control.Draw(drawPluginOptions);
    this.map.addControl(drawControl);

    this.map.on('draw:created', (e) => {
      var type = e.layerType,
      layer = e.layer;

      this.setState({
        layer: layer,
        areaDefined: true,
      });

      editableLayers.addLayer(layer);

    });

    let resp = await fetch(`/regions/get`);
    if(resp.status === 200){
      const data = await resp.json();
      this.setState({
        regions: data
      })
      console.log(this.state.regions)
      

    } else {
      const data = await resp.json();
      this.setState({
        error: data.message
      })
      
    }

    if(this.state.regions){

      for (let index = 0; index < this.state.regions.length; index++) {
        var geojsonFeature = {
          "type": "Feature",
          "properties": {
              "name": "Block",
              "amenity": "My block",
              "popupContent": "This is where I map buildings!",
          },
          "geometry": {
            "type": "Polygon",
            "coordinates": [this.state.regions[index].area]
          }
        };
        L.geoJSON(geojsonFeature, {"color": "gray"}).addTo(this.map);
      }

      resp = await fetch(`/blocks/get`);
      if(resp.status === 200){
        const data = await resp.json();
        this.setState({
          blocks: data
        })
        console.log(this.state.blocks)
  
      } else {
        const data = await resp.json();
        this.setState({
          error: data.message
        })
        
      }
  
      if(this.state.blocks){
  
        for (let index = 0; index < this.state.blocks.length; index++) {
          
          var geojsonFeature = {
            "type": "Feature",
            "properties": {
                "name": this.state.blocks[index].name,
                "amenity": "My block",
                "popupContent": "This is where I map buildings!"
            },
            "geometry": {
              "type": "Polygon",
              "coordinates": [this.state.blocks[index].area]
            }
          };
          var color;

          if(this.state.blocks[index].status === "AKTIVNO") {
            color = "red";
          } else if(this.state.blocks[index].status === "PROVJERA") {
            color = "yellow";
          } else if(this.state.blocks[index].status === "ZAVRŠENO") {
            color = "green";
          } else {
            color = "blue";
          }
          L.geoJSON(geojsonFeature, {"color":`${color}`}).addTo(this.map);
          
        }
  
      }
    }


  }

  handleClick = () => {
    const map = this.mapRef.current
    if (map != null) {
      map.leafletElement.locate()
    }
  }


  onSubmitRegion(e) {
    e.preventDefault();
    
    const region = {
      name: this.state.naziv,
      area: this.state.layer.toGeoJSON().geometry.coordinates[0],
    };

    console.log(JSON.stringify(region));

    const options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(region)
    };

    fetch('/leader/region/add', options);
  }

  async onSubmitBlock(e) {
    e.preventDefault();
    
    const block = {
      name: this.state.naziv,
      regionName: this.state.nazivRegija,
      area: this.state.layer.toGeoJSON().geometry.coordinates[0],
    };

    const options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(block)
    };

    const resp = await fetch('/leader/block/add', options);

    if(resp.status === 200){
      this.setState({
        blockError: ''
      })
    } else {
      const data = await resp.json();
      this.setState({
        blockError: data.message
      })
    }
  }

  async onSubmitBuilding(e) {
    e.preventDefault();
    
    const building = {
      name: this.state.naziv,
      blockName: this.state.nazivBlok,
      area: this.state.layer.toGeoJSON().geometry.coordinates[0],
    };

    const options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(building)
    };

    const resp = await fetch('/building/add', options);

    if(resp.status === 200){
      this.setState({
        buildingError: ''
      })
    } else {
      const data = await resp.json();
      this.setState({
        buildingError: data.message
      })
    }
  }

  render(){
    return (
      <div>
        <Wrapper width= "1280px" height="720px" id="map" />
        
        <button type="button" onClick={(e)=>{this.setState({
          defineRegion: !this.state.defineRegion,
          defineBlock: false,
          defineBuilding: false,
          naziv: ''
        });}}>Definiraj regiju</button>
        
        <button type="button" onClick={(e)=>{this.setState({
          defineRegion: false,
          defineBlock: !this.state.defineBlock,
          defineBuilding: false,
          naziv: ''
        });}}>Definiraj blok</button>
       
        <button type="button" onClick={(e)=>{this.setState({
          defineRegion: false,
          defineBlock: false,
          defineBuilding: !this.state.defineBuilding,
          naziv: ''

        });}}>Definiraj zgradu</button>
        
        <div className="Formular" hidden={!this.state.defineRegion}>
          <form onSubmit={this.onSubmitRegion.bind(this)}>
            <div className="FormularRedak">
              <label>Naziv regije:</label>
              <input name="naziv" onChange={(e)=>{this.setState({
                naziv: e.target.value,
              });}} value={this.state.naziv}/>
            </div>
            <button type="submit" disabled={!this.state.areaDefined}>Spremi</button>
          </form>
        </div>

        <div className="Formular" hidden={!this.state.defineBlock}>
          <form onSubmit={this.onSubmitBlock.bind(this)}>
            <div className="FormularRedak">
              <label>Naziv bloka:</label>
              <input name="naziv" onChange={(e)=>{this.setState({
                naziv: e.target.value,
              });}} value={this.state.naziv}/>
            </div>
            <div className="FormularRedak">
              <label>Naziv regije:</label>
              <input name="nazivRegija" onChange={(e)=>{this.setState({
                nazivRegija: e.target.value,
              });}} value={this.state.nazivRegija}/>
            </div>
            <div className='error'>{this.state.blockError}</div>
            <button type="submit" disabled={!this.state.areaDefined}>Spremi</button>
          </form>
        </div>

        <div className="Formular" hidden={!this.state.defineBuilding}>
          <form onSubmit={this.onSubmitBuilding.bind(this)}>
            <div className="FormularRedak">
              <label>Naziv zgrade:</label>
              <input name="naziv" onChange={(e)=>{this.setState({
                naziv: e.target.value,
              });}} value={this.state.naziv}/>
            </div>
            <div className="FormularRedak">
              <label>Naziv bloka:</label>
              <input name="nazivRegija" onChange={(e)=>{this.setState({
                nazivBlok: e.target.value,
              });}} value={this.state.nazivBlok}/>
            </div>
            <div className='error'>{this.state.buildingError}</div>
            <button type="submit" disabled={!this.state.areaDefined}>Spremi</button>
          </form>
        </div>
      </div>
    );
  }
}
