import React from "react";
import StatistikaBlokovi from "./StatistikaBlokovi";
import StatistikaOsobe from "./StatistikaOsobe";
import StatistikaGradevine from "./StatistikaGradevine";

function Statistika(props) {

  const [odabir, setOdabir] = React.useState("blokovi");

  function odabirBlokovi() {
    setOdabir("blokovi");
  }

  function odabirOsobe() {
    setOdabir("osobe");
  }

  function odabirGradevine() {
    setOdabir("gradevine");
  }

  return(
    <div>
      <button onClick={odabirBlokovi}>Blokovi</button>
      <button onClick={odabirOsobe}>Osobe</button>
      <button onClick={odabirGradevine}>Građevine</button>
      {odabir === "blokovi" && <StatistikaBlokovi/>}
      {odabir === "osobe" && <StatistikaOsobe/>}
      {odabir === "gradevine" && <StatistikaGradevine/>}
    </div>
  );

}

export default Statistika;
