import React from "react";
import ReactDOM from "react-dom";
import "./PodaciOsnovno.css";
import "./Podaci.css";

function Profil(props) {
  const [korisnik, setKorisnik] = React.useState({username: '', password: '', pictureURL: '', firstName: '', lastName: '', phoneNumber: '', email: '', role: ''});
  
  function promijeni() {
    props.history.push("/promjena");
  }

  const options={
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'

    },
    body: `username=${props.username}`
  }

  React.useEffect(() => {
    
    fetch('/user/info', options)
      .then(data => data.json())
      .then(korisnik => setKorisnik(korisnik))
      .then(console.log(korisnik))
    }, []);

  return(
    <div>
      <div className="PodaciOsnovno">
        <img src={korisnik.pictureURL}/>
        <h2>{korisnik.userName}</h2>
        <h3>{korisnik.role}</h3>
      </div>
      <div className="Podaci">
        <h2>Osobni podaci</h2>
        <h3>Ime:</h3>
        <label>{korisnik.firstName}</label>
        <h3>Prezime:</h3>
        <label>{korisnik.lastName}</label>
        <h3>Broj mobitela:</h3>
        <label>{korisnik.phoneNumber}</label>
        <h3>e-mail adresa:</h3>
        <label>{korisnik.email}</label>
      </div>
      <div className="Gumb">
        <button onClick={promijeni}>Promijeni podatke</button>
      </div>
    </div>
  );

}

export default Profil;
