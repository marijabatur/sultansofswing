import React from "react";
import {Link} from "react-router-dom";
import "./Header.css";

function HeaderNeprijavljen(props) {

  return (
    <header className="Header">
      <Link to="/">Naslovna</Link>
      <Link to="/nestali">Nestale osobe</Link>
      <Link to="/login">Prijava</Link>
      <Link to="/registration">Registracija</Link>
    </header>
  )

}

export default HeaderNeprijavljen;
