import React from "react";
import LineChart from "react-linechart";
import d3 from "d3";
import "./Statistika.css";

function StatistikaGradevine(props) {
  const [unsearchedPoints, setUnsearchedPoints] = React.useState([]);
  const [searchedPoints, setSearchedPoints] = React.useState([]);

  const [unsearchedColor, setUnsearchedColor] = React.useState("red");
  const [searchedColor, setSearchedColor] = React.useState("green");

  React.useEffect(() => {
      fetch(`building/statistics/unsearched`)
        .then(data => data.json())
        .then(points => setUnsearchedPoints(points))
  }, []);

  React.useEffect(() => {
    fetch(`building/statistics/searched`)
      .then(data => data.json())
      .then(points => setSearchedPoints(points))
  }, []);

 const buildingStatistics = [
   {
     name: "broj nepretraženih zgrada",
     color: unsearchedColor,
     points: unsearchedPoints,
   },
   {
     name: "broj pretraženih",
     color: searchedColor,
     points: searchedPoints,
   }
 ];

  return (
    <div>
      <div className="Statistika">
        <h3>Broj pretraženih i nepretraženih građevina kroz vrijeme</h3>
        <div className="StatistikaTablica">
          <LineChart
            hideXAxis = {false}
            hideYAxis={false}
            height={400}
            width={1150}
            interpolate={"linear"}
            xDisplay={d3.time.format("%d.%m.%Y")}
            yMin={"0"}
            ticks={10}
            isDate={true}
            data={buildingStatistics}
            xLabel={"datum"}
            yLabel={"broj zgrada"}
            legendPosition={"top-center"}
            showLegends={true}
          />
        </div>
      </div>
    </div>
 
  );

}

export default StatistikaGradevine;
