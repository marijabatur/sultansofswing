import React, {Component} from "react";
import L from 'leaflet';
import 'leaflet/dist/leaflet.css';
import 'leaflet-draw';
import 'leaflet-draw/dist/leaflet.draw.css';
import styled from 'styled-components';
import 'geojson';

const Wrapper = styled.div`
  width: ${props => props.width};
   height: ${props => props.height};
`;

var GeoJSON = require('geojson');

export default class KartaSpasioc extends Component {


  constructor(props) {
    super(props);
    this.state = {
      username: props.username,
      uloga:props.uloga,
      nazivZgrada: '',
      buildingError: '',
      chosenBuilding: null,
    }
  }
  onMapClick(e) {
    var popup = L.popup();
    popup.setLatLng(e.latlng).setContent('You clicked the map' + e.latlng.toString()).openOn(e.target);
  }

  onClick(e) {
    this.setState({
      chosenBuilding: e.target,
    });
    e.target.setStyle({color: 'black'});
    console.log(e.target);
  }

  async componentDidMount() {
    this.map = L.map('map', {
      center: [46,16],
      zoom: 7,
      zoomControl: false,
    });

    L.tileLayer('https://api.maptiler.com/maps/streets/{z}/{x}/{y}.png?key=yCR2MJCLGXABQBsbaOR9', {
      attribution: '<a href="https://www.maptiler.com/copyright/" target="_blank">&copy; MapTiler</a> <a href="https://www.openstreetmap.org/copyright" target="_blank">&copy; OpenStreetMap contributors</a>',
      detectRetina: true,
      maxZoom: 50,
      maxNativeZoom: 17,
      drawControl: true,
    }).addTo(this.map);
      
    var center = [52.520, 13.405];
    L.marker(center).addTo(this.map);
  
    let resp = await fetch(`/buildings/unsearched/get`);
    if(resp.status === 200){
      const data = await resp.json();
      this.setState({
        buildings: data,
      })

        for (let index = 0; index < this.state.buildings.length; index++) {
            
            var geojsonFeature = {
                "type": "Feature",
                "properties": {
                    "name": this.state.buildings[index].name,
                    "amenity": "My block",
                    "popupContent": "This is where I map buildings!"
                },
                "geometry": {
                    "type": "Polygon",
                    "coordinates": [this.state.buildings[index].area]
                }
            };

            var layer = L.geoJSON(geojsonFeature, {
                "color":"red",
                "name":`${this.state.buildings[index].name}`
            });

            layer.addTo(this.map);
            layer.on('click', this.onClick.bind(this));
        }
    } else {
      const data = await resp.json();
      this.setState({
        error: data.message,
      });
    }

    var editableLayers = new L.FeatureGroup();
    this.map.addLayer(editableLayers);
  
    var drawPluginOptions = {
      position: 'topright',
      draw: {
        polygon: {
          allowIntersection:false,
          drawError: {
            color: '#e1e100',
            message: 'You can\'t draw that!'
          },
          shapeOptions: {
            color: '#97009c'
          },
            
        },
  
        polyline: false,
        circle: false,
        rectangle: false,
        marker: false,
        circlemarker: false,
          
      },
      edit: {
        featureGroup: editableLayers,
        remove: false
      }
    };
  
    var drawControl = new L.Control.Draw(drawPluginOptions);
    this.map.addControl(drawControl);
    var geojson= new L.GeoJSON();

    this.map.on('draw:created', (e) => {
      let layer = e.layer;
       
      this.setState({
        layer: layer,
        areaDefined:true,
      });
  
      editableLayers.addLayer(layer);
    });  
      
  }
  
  oznaciKaoPretrazeno(e) {
    e.preventDefault();
    
    console.log(this.state.chosenBuilding.options.name);

    const options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      body: `name=${this.state.chosenBuilding.options.name}`
    };

    this.setState({
      nazivZgrada: this.state.chosenBuilding.options.name 
    });

    this.state.chosenBuilding.setStyle({color: 'green'});

    fetch('/buildings/searched/set', options);
    this.props.history.push("/kartaSpasioc");
  }

    handleClick = () => {
        const map = this.mapRef.current
        if (map != null) {
        map.leafletElement.locate()
        }
    }

  render() {
    return(
      <div>
        <div className='error'>{this.state.error}</div>
        <Wrapper width= "1280px" height="720px" id="map" />

        <button type="button" onClick={this.oznaciKaoPretrazeno.bind(this) }>Označi zgradu kao pretraženu</button>
        <div className='error'>{this.state.buildingError}</div>
      </div>
    )
  }
}