import React from "react";
import NeregistriraniKorisnik from "./NeregistriraniKorisnik";

function NeregistriraniKorisnici() {

  const [korisnici, setKorisnici] = React.useState([]);

  React.useEffect(() => {
    fetch('/admin/view')
      .then(data => data.json())
      .then(korisnici => setKorisnici(korisnici))
  }, []);

  return (
    <div>
      {
        korisnici.map(korisnik =>
          <NeregistriraniKorisnik key={korisnik.id} neregistriraniKorisnik={korisnik}/>
       )
      }
    </div>
  );

}

export default NeregistriraniKorisnici;
