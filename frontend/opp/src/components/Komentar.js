import React from "react";
import  "./Komentar.css";

function Komentar(props) {

  const {id, text} = props.komentar;

  function obrisi() {

  }

  return (
    <div className="Komentar">
      {props.uloga && ((props.uloga === "voditelj") || (props.uloga === "administrator")) && <button type="submit" onClick = {obrisi}>Obriši</button>}
      <p>{text}</p>
    </div>
  )

}

export default Komentar;
