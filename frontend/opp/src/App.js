import React from "react";
import {BrowserRouter, Switch, Route} from "react-router-dom";
import ReactDOM from "react-dom";

import Header from "./components/Header";
import Naslovna from "./components/Naslovna";
import Prijava from "./components/Prijava";
import Registracija from "./components/Registracija";
import Zahtjevi from "./components/Zahtjevi";

import Odjava from "./components/Odjava";
import RegistracijaZaprimljena from "./components/RegistracijaZaprimljena";

import NestaleOsobe from "./components/NestaleOsobe";
import Korisnici from "./components/Korisnici";
import NestalaOsobaPrijava from "./components/NestalaOsobaPrijava";
import KartaVoditelj from "./components/KartaVoditelj";
import Profil from "./components/Profil";
import Promjena from "./components/Promjena";
import Statistika from "./components/Statistika";
import KartaKartograf from "./components/KartaKartograf";
import KartaSpasioc from "./components/KartaSpasioc";

class App extends React.Component {

  constructor(props){
    super(props);
    this.state = {
      uloga: null,
    };
  }

  setUloga(role, username) {
    this.setState({
      uloga: role,
      username: username
    });
  }

  render() {
    return(

      <BrowserRouter>
       <div className="App">
          <Header setRole={(role) => this.setUloga.bind(this)} uloga={ this.state.uloga}/>

          <Switch>
              <Route path="/" exact render={(props) =>
                <Naslovna {...props}
                  setRole={this.setUloga.bind(this)}
                  uloga={this.state.uloga}
                />}
              />

                {!this.state.uloga && <Route path="/login"
                  exact render={(props) =>
                    <Prijava {...props} setRole={this.setUloga.bind(this)}
                  />}
                />} //component={Prijava}/>}
                {!this.state.uloga && <Route path="/registracija" exact component={Registracija}/>}
                {this.state.uloga && (this.state.uloga === "administrator") && <Route path="/zahtjevi" exact component={Zahtjevi}/>}
              <Route path="/logout" exact render={
                (props) =>
                  <Odjava {...props} setRole={this.setUloga.bind(this)}/>
              }/>
              <Route path="/zaprimljeno" exact component={RegistracijaZaprimljena}/>
              {this.state.uloga && (this.state.uloga === "administrator") && <Route path="/korisnici" exact component={Korisnici}/>}
              <Route path="/nestali" exact render={(props) =>
                <NestaleOsobe {...props}
                  setRole={this.setUloga.bind(this)}
                  uloga={this.state.uloga}
                />}
              />
              <Route path="/prijavi" exact component={NestalaOsobaPrijava}/>
              <Route path="/kartaVoditelj" exact render={(props) =>
                <KartaVoditelj {...props}
                  setRole={this.setUloga.bind(this)}
                  uloga={this.state.uloga}
                  username={this.state.username}
                />}
              />
              <Route path="/kartaKartograf" exact render={(props) =>
                <KartaKartograf {...props}
                  setRole={this.setUloga.bind(this)}
                  uloga={this.state.uloga}
                  username={this.state.username}
                />}
              />
              <Route path="/kartaSpasioc" exact render={(props) =>
                <KartaSpasioc {...props}
                  setRole={this.setUloga.bind(this)}
                  uloga={this.state.uloga}
                  username={this.state.username}
                />}
              />
              {this.state.uloga && <Route path="/profil" exact render={(props) =>
                <Profil {...props}
                  setRole={this.setUloga.bind(this)}
                  uloga={this.state.uloga}
                  username={this.state.username}
                />}/>}
              {this.state.uloga && <Route path="/promjena" exact render={(props) =>
                <Promjena {...props}
                  setRole={this.setUloga.bind(this)}
                  uloga={this.state.uloga}
                  username={this.state.username}
                />}/>}
                {this.state.uloga && ((this.state.uloga === "administrator") || (this.state.uloga === "voditelj") || (this.state.uloga === "kartograf")) &&
                <Route path="/statistika" exact render={(props) =>
               <Statistika {...props}
                 setRole={this.setUloga.bind(this)}
                 uloga={this.state.uloga}
                 username={this.state.username}
               />}/>
             }
          </Switch>

       </div>
      </BrowserRouter>
    );
  }

}

ReactDOM.render(
  <App />,
  document.getElementById('root')
);

  export default App;
