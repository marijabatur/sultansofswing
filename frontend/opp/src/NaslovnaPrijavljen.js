import React from "react";
import "./Naslovna.css";

function NaslovnaPrijavljen() {

  return (
    <div className="Naslovna">
      <h1>Humanitarno kartografiranje</h1>
      <h2>Prijavljen korisnik</h2>
    </div>
  )

}

export default NaslovnaPrijavljen;