package SultansOfSwing.HumanitarnoKartografiranje.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import SultansOfSwing.HumanitarnoKartografiranje.DTO.BuildingDTO;
import SultansOfSwing.HumanitarnoKartografiranje.DTO.StatisticsDTO;
import SultansOfSwing.HumanitarnoKartografiranje.service.IAreaService;

@RestController
public class BlockController {
	
	@Autowired
	private IAreaService service;
	
	@GetMapping("/block/statistics")
	public List<StatisticsDTO> getFinishedBlockStatistics() {
		return service.getBlockStatistics();
	}
	
	@GetMapping("/building/statistics/unsearched")
	public List<StatisticsDTO> getUnsearchedBuildingStatistics() {
		return service.getUnsearchedBuildingStatistics();
	}
	
	@GetMapping("/building/statistics/searched")
	public List<StatisticsDTO> getSearchedBuildingStatistics() {
		return service.getSearchedBuildingStatistics();
	}
	
	@PostMapping("/building/add")
	public ResponseEntity<String> addBuilding(@RequestBody BuildingDTO building) {
		try {
			service.addBuilding(building);
		} catch (Exception e) {
			return ResponseEntity.badRequest().body("{\"message\":\"" + e.getLocalizedMessage() + "\"}");
		}
		return ResponseEntity.ok().body("{\"message\":\"Building successfully added.\"}");
	}
	
	@GetMapping("/buildings/get")
	public ResponseEntity<?> getBuildings(@RequestParam(name="blockName") String blockName) {
		try {
			return ResponseEntity.ok().body(service.getBuildings(blockName));
		} catch (Exception e) {
			return ResponseEntity.badRequest().body("{\"message\":\"" + e.getLocalizedMessage() + "\"}");
		}
	}
	
	@GetMapping("/cartographer/block/get")
	public ResponseEntity<?> getBlock(@RequestParam(name="username") String username) {
		try {
			return ResponseEntity.ok(service.getBlock(username));
		} catch (Exception e) {
			return ResponseEntity.badRequest().body("{\"message\":\"" + e.getLocalizedMessage() + "\"}");
		}
	}
	
	@PostMapping("/cartographer/block/set")
	public ResponseEntity<?> setBlock(@RequestParam(name="username") String username, @RequestParam(name="blockName") String blockName) {
		try {
			service.setBlock(username, blockName);
			return ResponseEntity.ok("{\"message\":\"Block successfully set.\"}");
		} catch (Exception e) {
			return ResponseEntity.badRequest().body("{\"message\":\"" + e.getLocalizedMessage() + "\"}");
		}
	}
	
	@PostMapping("/cartographer/block/approve")
	public ResponseEntity<?> approveBlock(@RequestParam(name="username") String username, @RequestParam(name="blockName") String blockName) {
		try {
			service.approveBlock(username, blockName);
			return ResponseEntity.ok("{\"message\":\"Block approved.\"}");
		} catch (Exception e) {
			return ResponseEntity.badRequest().body("{\"message\":\"" + e.getLocalizedMessage() + "\"}");
		}
	}
	
	@PostMapping("/cartographer/block/disapprove")
	public ResponseEntity<?> disapproveBlock(@RequestParam(name="username") String username, @RequestParam(name="blockName") String blockName) {
		try {
			service.disapproveBlock(username, blockName);
			return ResponseEntity.ok("{\"message\":\"Block disapproved.\"}");
		} catch (Exception e) {
			return ResponseEntity.badRequest().body("{\"message\":\"" + e.getLocalizedMessage() + "\"}");
		}
	}
	
	@PostMapping("/cartographer/block/finish")
	public ResponseEntity<?> finishBlock(@RequestParam(name="username") String username, @RequestParam(name="blockName") String blockName) {
		try {
			service.finishBlock(username, blockName);
			return ResponseEntity.ok("{\"message\":\"Block finished.\"}");
		} catch (Exception e) {
			return ResponseEntity.badRequest().body("{\"message\":\"" + e.getLocalizedMessage() + "\"}");
		}
	}
	
	@GetMapping("/blocks/get")
	public ResponseEntity<?> getBlocks() {
		try {
			return ResponseEntity.ok(service.getBlocks());
		} catch (Exception e) {
			return ResponseEntity.badRequest().body("{\"message\":\"" + e.getLocalizedMessage() + "\"}");
		}
	}
	
	@GetMapping("/blocks/get/notStarted")
	public ResponseEntity<?> getFreeBlocks() {
		try {
			return ResponseEntity.ok(service.getFreeBlocks());
		} catch (Exception e) {
			return ResponseEntity.badRequest().body("{\"message\":\"" + e.getLocalizedMessage() + "\"}");
		}
	}
	
	@GetMapping("/regions/get")
	public ResponseEntity<?> getRegions() {
		try {
			return ResponseEntity.ok(service.getRegions());
		} catch (Exception e) {
			return ResponseEntity.badRequest().body("{\"message\":\"" + e.getLocalizedMessage() + "\"}");
		}
	}
	
	@GetMapping("/buildings/unsearched/get")
	public ResponseEntity<?> getUnsearchedBuildings() {
		try {
			return ResponseEntity.ok().body(service.getUnsearchedBuildings());
		} catch (Exception e) {
			return ResponseEntity.badRequest().body("{\"message\":\"" + e.getLocalizedMessage() + "\"}");
		}
	}
	
	@PostMapping("/buildings/searched/set")
	public ResponseEntity<?> setSearchedBuilding(@RequestParam(name="name") String buildingName) {
		try {
			service.setSearchedBuilding(buildingName);
			return ResponseEntity.ok().body("{\"message\":\"Building successfully added to searched.\"}");
		} catch (Exception e) {
			return ResponseEntity.badRequest().body("{\"message\":\"" + e.getLocalizedMessage() + "\"}");
		}
	}

	@GetMapping("/blocks/get/check")
	public ResponseEntity<?> getCheckBlocks() {
		try {
			return ResponseEntity.ok(service.getCheckBlocks());
		} catch (Exception e) {
			return ResponseEntity.badRequest().body("{\"message\":\"" + e.getLocalizedMessage() + "\"}");
		}
	}
}
