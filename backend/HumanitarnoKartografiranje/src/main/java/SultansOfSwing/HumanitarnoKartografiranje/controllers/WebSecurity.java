package SultansOfSwing.HumanitarnoKartografiranje.controllers;

import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import SultansOfSwing.HumanitarnoKartografiranje.service.UserMainDetailsService;

@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class WebSecurity extends WebSecurityConfigurerAdapter {

	private UserMainDetailsService userMainDetailsService;

	public WebSecurity(UserMainDetailsService userMainDetailsService) {
		this.userMainDetailsService = userMainDetailsService;
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) {
		auth.authenticationProvider(authenticationProvider());
	}


	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.httpBasic();
		
		http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
		
		http.authorizeRequests()
		.antMatchers("/").permitAll()
		.antMatchers("/registration").permitAll()
		.antMatchers("/admin/**").hasRole("administrator")
		.antMatchers("/leader/**").hasAnyRole("voditelj", "administrator")
		.antMatchers("/rescuer/**").hasAnyRole("spasioc", "administrator")
		.antMatchers("/cartographer/**").hasAnyRole("kartograf", "administrator")
		.antMatchers("**/lock/**").hasAnyRole("spasioc", "voditelj")
		.antMatchers("**/statistics/**").hasAnyRole("kartograf", "voditelj", "administrator")
		.and()
		.formLogin()
		.loginProcessingUrl("/authLogin")  //ovaj redak obrisat ako je u kodu frontendu prijava nazvana s login ili zamjenit riječ "signin" u naziv akcije   
		.loginPage("/login").permitAll()
		.usernameParameter("userName") //ovaj redak obrisat ako je u kodu frontenda id i name korisnika nazvan username ili zamjeniti riječ "txtUsername" s riječi kojom je nazvan id i name korisnika koji se prijavljuje 
		.and()
		.logout().logoutRequestMatcher(new AntPathRequestMatcher("/logout")).logoutSuccessUrl("/login");
		
		http.csrf().disable();
	}

	@Bean
	DaoAuthenticationProvider authenticationProvider(){
		DaoAuthenticationProvider daoAuthenticationProvider = new DaoAuthenticationProvider();
		daoAuthenticationProvider.setPasswordEncoder(passwordEncoder());
		daoAuthenticationProvider.setUserDetailsService(this.userMainDetailsService);

		return daoAuthenticationProvider;
	}

	@Bean
	PasswordEncoder passwordEncoder() {
		return PasswordEncoderFactories.createDelegatingPasswordEncoder();
	}

}
