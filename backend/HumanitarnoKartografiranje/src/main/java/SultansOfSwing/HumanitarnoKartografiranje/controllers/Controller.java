

package SultansOfSwing.HumanitarnoKartografiranje.controllers;

import java.net.URI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import SultansOfSwing.HumanitarnoKartografiranje.DTO.RegistrationDTO;
import SultansOfSwing.HumanitarnoKartografiranje.model.User;
import SultansOfSwing.HumanitarnoKartografiranje.service.LoginService;
import SultansOfSwing.HumanitarnoKartografiranje.service.RegisterService;

@RestController
public class Controller {
	
	@Autowired
	private RegisterService registerService;
	
	@Autowired
	private LoginService loginService;
	
	@PostMapping("/registration") 
	public ResponseEntity<?> createUser(@RequestBody RegistrationDTO user) {
		User new_2;
		try {
			new_2 = registerService.registerUser(user);
		} catch (Exception e) {
			return ResponseEntity.badRequest().body("{\"message\":\""+e.getMessage()+"\"}");
		}
		
		return ResponseEntity.created(
				URI.create("/registration" + new_2.getId())).body(new_2);
	}
	
	@PostMapping("/addPicture")
	public ResponseEntity<?> addPicture(@RequestParam(name="picture") MultipartFile picture){
		String username = picture.getOriginalFilename();

		try {
			registerService.addPicture(picture, username);
		} catch (Exception e) {
			return ResponseEntity.badRequest().body("{\"message\":\"Error adding picture.\"}");
		}
		return ResponseEntity.ok().body("{\"message\":\"Picture successfully added.\"}");
	}
	
	@PostMapping("/login")
	public ResponseEntity<?> checkMessage(@RequestParam(name = "userName") String userName,
			@RequestParam(name = "password") String password) {
		RegistrationDTO user;
		try {
			user = loginService.checkLogin(userName, password);
			
		} catch(Exception e) {
			return ResponseEntity.badRequest().body("message:"+e.getLocalizedMessage());
		}
		
		return ResponseEntity.ok().body(user);
	}
	
	@PostMapping("/user/info")
	public ResponseEntity<?> getUserDetail(@RequestParam(name="username") String username){
		return ResponseEntity.ok().body(registerService.getUserDetails(username));
	}
	
	@PostMapping("/user/change")
	public ResponseEntity<?> changeUserDetail(@RequestParam(name="username") String username,
			@RequestParam(name="password") String password,
			@RequestParam(name="firstName") String firstName,
			@RequestParam(name="lastName") String lastName,
			@RequestParam(name="email") String email,
			@RequestParam(name="phoneNumber") String phoneNumber) {
		
		try {
			registerService.changeUserDetail(username, password, firstName, lastName, email, phoneNumber);
		} catch (Exception e) {
			return ResponseEntity.badRequest().body("message:" + e.getLocalizedMessage());
		}
		return ResponseEntity.ok().body("{\"message\":\"User details successfully changed.\"}");
	}
}