package SultansOfSwing.HumanitarnoKartografiranje.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import SultansOfSwing.HumanitarnoKartografiranje.DTO.BlockDTO;
import SultansOfSwing.HumanitarnoKartografiranje.DTO.RegionDTO;
import SultansOfSwing.HumanitarnoKartografiranje.service.IAreaService;

@RestController
@RequestMapping("/leader")
public class TeamLeaderController {
	
	@Autowired
	private IAreaService areaService;
	
	@PostMapping("/region/add")
	public ResponseEntity<String> defineRegion(@RequestBody RegionDTO region) {
		try {
			areaService.defineRegion(region);
		} catch (Exception e) {
			return ResponseEntity.badRequest().body("{\"message\":\"" + e.getLocalizedMessage() + "\"}");
		}
		return ResponseEntity.ok().body("{\"message\":\"Region successfully added.\"}");
	}
	
	@PostMapping("/block/add")
	public ResponseEntity<String> defineBlock(@RequestBody BlockDTO block) {
		try {
			areaService.defineBlock(block);
		} catch (Exception e) {
			return ResponseEntity.badRequest().body("{\"message\":\"" + e.getLocalizedMessage() + "\"}");
		}
		return ResponseEntity.ok().body("{\"message\":\"Region successfully added.\"}");
	}

}
