package SultansOfSwing.HumanitarnoKartografiranje.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import SultansOfSwing.HumanitarnoKartografiranje.DTO.CommentDTO;
import SultansOfSwing.HumanitarnoKartografiranje.DTO.MissingPersonDTO;
import SultansOfSwing.HumanitarnoKartografiranje.DTO.StatisticsDTO;
import SultansOfSwing.HumanitarnoKartografiranje.service.IMissingPersonService;

@RestController
@RequestMapping("/missingPerson")
public class MissingPersonController {
	
	@Autowired
	private IMissingPersonService service;
	
	@GetMapping("/view")
	public List<MissingPersonDTO> getMissingPeople() {
		return service.getAllMissingPeople();
	}
	
	@GetMapping("/comments")
	public List<CommentDTO> getComments(@RequestParam(name="id") Long id) {
		try {
			return service.getComments(id);
		} catch (Exception e) {
			return new ArrayList<>();
		}
	}
	
	@PostMapping("/comments/add")
	public ResponseEntity<String> addComment(@RequestParam(name="id") Long id, @RequestParam(name="comment") String commentText) {
		try {
			service.addComment(id, commentText);
		} catch (Exception e) {
			return ResponseEntity.badRequest().body("{\"message\":\""+e.getMessage()+"\"}");
		}
		return ResponseEntity.ok().body("{\"message\":\"Comment successfully added.\"}");
	}
	
	@PostMapping("/leader/comments/delete")
	public ResponseEntity<String> deleteComment(@RequestParam(name="idPerson") Long idPerson,
			@RequestParam(name="idComment") Long idComment) {
		
		try {
			service.deleteComment(idPerson, idComment);
		} catch (Exception e) {
			return ResponseEntity.badRequest().body("{\"message\":\""+e.getMessage()+"\"}");
		}
		return ResponseEntity.ok().body("{\"message\":\"Comment successfully deleted.\"}");
	}
	
	@PostMapping("/lock")
	public ResponseEntity<String> lockMissingPersonReport(@RequestParam(name="id") Long id) {
		try {
			service.lockReport(id);
		} catch (Exception e) {
			return ResponseEntity.badRequest().body("{\"message\":\""+e.getMessage()+"\"}");
		}
		return ResponseEntity.ok().body("{\"message\":\"Missing person report successfully locked.\"}");
	}
	
	@PostMapping("/leader/delete")
	public ResponseEntity<String> deleteMissingPersonReport(@RequestParam(name="id") Long id) {
		try {
			service.deleteReport(id);
		} catch (Exception e) {
			return ResponseEntity.badRequest().body("{\"message\":\""+e.getMessage()+"\"}");
		}
		return ResponseEntity.ok().body("{\"message\":\"Missing person report successfully deleted.\"}");
	}
	
	@PostMapping("/report")
	public ResponseEntity<String> reportMissingPerson(@RequestBody MissingPersonDTO missingPerson) {
		try {
			long id = service.reportMissingPerson(missingPerson);
			return ResponseEntity.ok().body("{\"id\":\""+ id +"\"}");
		} catch (Exception e) {
			return ResponseEntity.badRequest().body("{\"message\":\""+e.getMessage()+"\"}");
		}
		
	}
	
	@PostMapping("/addPicture")
	public ResponseEntity<?> addPicture(@RequestParam(name="picture") MultipartFile picture){
		String id = picture.getOriginalFilename();

		try {
			service.addPicture(picture, Long.parseLong(id));
		} catch (Exception e) {
			return ResponseEntity.badRequest().body("{\"message\":\"" + e.getMessage() + "\"}");
		}
		return ResponseEntity.ok().body("{\"message\":\"Picture successfully added.\"}");
	}
	
	@GetMapping("/statistics/reported")
	public List<StatisticsDTO> getRepotedPeopleStatistics() {
		return service.getRepotedPeopleStatistics();
	}
	
	@GetMapping("/statistics/found")
	public List<StatisticsDTO> getFoundPeopleStatistics() {
		return service.getFoundPeopleStatistics();
	}
}
