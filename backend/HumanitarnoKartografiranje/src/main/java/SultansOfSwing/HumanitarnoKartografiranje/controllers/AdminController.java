package SultansOfSwing.HumanitarnoKartografiranje.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import SultansOfSwing.HumanitarnoKartografiranje.model.User;
import SultansOfSwing.HumanitarnoKartografiranje.service.RegisterService;


@RestController
@RequestMapping("/admin")
public class AdminController {
	
	@Autowired
	private RegisterService service;
	
	@GetMapping("/view")
	public List<User> listUnregisteredUsers() {
	    return service.viewRegistrationRequests();
	}

	@PostMapping("/view/accept")
	public void approveRequest(@RequestBody User user) { 
	    try {
			service.acceptUser(user);
		} catch (Exception e) {}                       
	}
	  
	@PostMapping("/view/decline")
	public void declineRequest(@RequestBody User user) { 
	    try {
			service.declineUser(user);
		} catch (Exception e) {}                       
	}
	
	@GetMapping("/users")
	public List<User> listAllUsers() {
	    return service.listAllUsers();
	}
	
	@PostMapping("/role")
	public ResponseEntity<?> changeRole(@RequestParam(name = "username") String username,
			@RequestParam(name = "role") String role) {
		
		try {
			service.changeRole(username, role);
		} catch (Exception e) {
			return ResponseEntity.badRequest().body("{\"message\":\"" + e.getLocalizedMessage() + "\"}");
		}
		return ResponseEntity.ok().body("{\"message\":\"Role successfully updated.\"}");
	}
}
