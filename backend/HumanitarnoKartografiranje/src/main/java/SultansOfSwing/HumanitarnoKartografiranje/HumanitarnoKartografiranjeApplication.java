package SultansOfSwing.HumanitarnoKartografiranje;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HumanitarnoKartografiranjeApplication {

	public static void main(String[] args) {
		SpringApplication.run(HumanitarnoKartografiranjeApplication.class, args);
	}

}
