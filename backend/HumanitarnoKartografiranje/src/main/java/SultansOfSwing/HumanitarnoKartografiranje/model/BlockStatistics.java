package SultansOfSwing.HumanitarnoKartografiranje.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name="StatistikaBlokovi")
public class BlockStatistics {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	@Column(nullable=false, unique= true)
	private String date;
	
	@Column
	private int numberOfFinished;

	public BlockStatistics() {
		super();
		numberOfFinished = 0;
	}

	public BlockStatistics(String date, int numberOfFinished) {
		super();
		this.date = date;
		this.numberOfFinished = numberOfFinished;
	}

	public long getId() {
		return id;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public int getNumberOfFinished() {
		return numberOfFinished;
	}

	public void addNumberOfFinished(int numberOfFinished) {
		this.numberOfFinished += numberOfFinished;
	}

}
