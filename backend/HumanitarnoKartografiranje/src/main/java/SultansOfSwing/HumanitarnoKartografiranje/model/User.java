package SultansOfSwing.HumanitarnoKartografiranje.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name="Korisnik")
public class User {
	
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name="korisnickoIme", unique=true, nullable=false)
	private String userName;
	
	@Column(name="lozinka", nullable=false)
	private String password;
	
	@Column(name="ime", nullable=false)
	private String firstName;
	
	@Column(name="prezime", nullable=false) 	//possibly put nullable as true, and assuring existence of 
	private String lastName;				//data in service layer
	
	@Column(name="slika")
	private String pictureURL;
	
	@Column(name="brojMobitela")
	private String phoneNumber;
	
	@Column(nullable=false)
	private String email;
	
	@Column(name="uloga", nullable = false)
	private String role;
	
	@Column(name="registriran", updatable=true)
	private boolean registered;
	
	@OneToMany(mappedBy="cartographer", fetch=FetchType.LAZY, cascade=CascadeType.PERSIST, orphanRemoval=true)
	private List<BlockCheck> blocks = new ArrayList<>();

	public User() {
		super();
	}

	public User(String userName, String password, String firstName, String lastName, String pictureURL,
			String phoneNumber, String email, String role) {
		super();
		this.userName = Objects.requireNonNull(userName);
		this.password = Objects.requireNonNull(password);
		this.firstName = Objects.requireNonNull(firstName);
		this.lastName = Objects.requireNonNull(lastName);
		this.pictureURL = pictureURL;
		this.phoneNumber = phoneNumber;
		this.email = Objects.requireNonNull(email);
		this.role = Objects.requireNonNull(role);
		this.registered = false;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public boolean isRegistered() {
		return registered;
	}

	public void setRegistered(boolean registered) {
		this.registered = registered;
	}

	public long getId() {
		return id;
	}

	public String getUserName() {
		return userName;
	}

	public String getPassword() {
		return password;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getPictureURL() {
		return pictureURL;
	}

	public void setPictureURL(String pictureURL) {
		this.pictureURL= pictureURL;
	}
	public String getEmail() {
		return email;
	}

	public String getRole() {
		return role;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public List<BlockCheck> getBlocks() {
		return blocks;
	}
}
