package SultansOfSwing.HumanitarnoKartografiranje.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name="StatistikaNestaleOsobe")
public class MissingPersonStatistics {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	@Column(nullable=false, unique= true)
	private String date;
	
	@Column
	private int numberOfReported;
	
	@Column
	private int numberOfFound;

	public MissingPersonStatistics() {
		super();
		numberOfFound = 0;
		numberOfReported = 0;
	}
	
	

	public MissingPersonStatistics(String date, int numberOfReported, int numberOfFound) {
		super();
		this.date = date;
		this.numberOfReported = numberOfReported;
		this.numberOfFound = numberOfFound;
	}



	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public int getNumberOfReported() {
		return numberOfReported;
	}

	public void setNumberOfReported(int numberOfReported) {
		this.numberOfReported = numberOfReported;
	}

	public int getNumberOfFound() {
		return numberOfFound;
	}

	public void setNumberOfFound(int numberOfFound) {
		this.numberOfFound = numberOfFound;
	}

	public long getId() {
		return id;
	}

}
