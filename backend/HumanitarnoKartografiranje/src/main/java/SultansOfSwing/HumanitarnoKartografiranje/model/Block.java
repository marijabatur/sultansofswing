package SultansOfSwing.HumanitarnoKartografiranje.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.Polygon;

@Entity(name="Blok")
public class Block {
	
	@Column
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	@Column(name="nazivBlok")
	private String name;
	
	@Column(name="statusBlok", nullable=false)
	private BlockStatus status;
	
	@Column(name="podrucjeBlok", nullable= false, columnDefinition="POLYGON")
	private Polygon area;
	
	@ManyToOne
	@JoinColumn(nullable=false)
	private Region region;
	
	@OneToMany(mappedBy="block", fetch=FetchType.LAZY, cascade=CascadeType.PERSIST, orphanRemoval= true)
	private List<Building> buildings = new ArrayList<>();
	
	@OneToOne(mappedBy="block", fetch=FetchType.LAZY, cascade=CascadeType.PERSIST, orphanRemoval= true)
	private BlockCheck assignedCartographer;
	
	
	public Block(String name, List<List<Double>> area) {
		if(area.size() < 3) throw new IllegalArgumentException("Valid polygon must have at least 3 points.");
		
		this.name = name;
		this.status = BlockStatus.NEZAPOČETO;
		
		Coordinate[] points = new Coordinate[area.size()];
		int i = 0;
		for (List<Double> list : area) {

			if(list.size() != 2) throw new IllegalArgumentException("Invalid point.");
			points[i] = new Coordinate(list.get(0), list.get(1));
			i++;

		}
		GeometryFactory gf = new GeometryFactory();
		this.area = gf.createPolygon(points);
	}


	public Block() {
		super();
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public BlockStatus getStatus() {
		return status;
	}


	public void setStatus(BlockStatus status) {
		this.status = status;
	}


	public Polygon getArea() {
		return area;
	}


	public void setArea(Polygon area) {
		this.area = area;
	}


	public Region getRegion() {
		return region;
	}


	public void setRegion(Region region) {
		this.region = region;
	}


	public List<Building> getBuildings() {
		return buildings;
	}


	public void setBuildings(List<Building> buildings) {
		this.buildings = buildings;
	}


	public long getId() {
		return id;
	}


	public BlockCheck getAssignedCartographer() {
		return assignedCartographer;
	}


	public void setAssignedCartographer(BlockCheck assignedCartographer) {
		this.assignedCartographer = assignedCartographer;
	}
	
}
