package SultansOfSwing.HumanitarnoKartografiranje.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.Polygon;

@Entity(name="Regija")
public class Region {
	
	@Column
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name="nazivRegija", unique=true)
	private String name;
	
	@Column(name="podrucjeRegija")
	private Polygon area;
	
	@OneToMany(mappedBy="region",fetch=FetchType.LAZY, cascade=CascadeType.PERSIST, orphanRemoval=true)
	private List<Block> blocks = new ArrayList<>();

	public Region() {
		super();
	}

	public Region(String name, List<List<Double>> area) {
		super();
		if(area.size() < 3) throw new IllegalArgumentException("Valid polygon must have at least 3 points.");
		
		this.name = name;		
		
		Coordinate[] points = new Coordinate[area.size()];
		int i = 0;
		for (List<Double> list : area) {

			if(list.size() != 2) throw new IllegalArgumentException("Invalid point.");
			points[i] = new Coordinate(list.get(0), list.get(1));
			i++;

		}
		GeometryFactory gf = new GeometryFactory();
		this.area = gf.createPolygon(points);
	}

	public Polygon getArea() {
		return area;
	}

	public void setArea(Polygon area) {
		this.area = area;
	}

	public List<Block> getBlocks() {
		return blocks;
	}

	public void setBlocks(List<Block> blocks) {
		this.blocks = blocks;
	}

	public long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((area == null) ? 0 : area.hashCode());
		result = prime * result + ((blocks == null) ? 0 : blocks.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Region other = (Region) obj;
		if (id != other.id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	
}
