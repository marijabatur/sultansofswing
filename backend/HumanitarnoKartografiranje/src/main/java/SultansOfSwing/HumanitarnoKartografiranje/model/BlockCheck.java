package SultansOfSwing.HumanitarnoKartografiranje.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity(name="provjeraBlok")
public class BlockCheck {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	@ManyToOne
	@JoinColumn(nullable=false)
	private User cartographer;
	
	@OneToOne
	@JoinColumn(nullable=false)
	private Block block;
	
	@Column(name="ProvjeriloKartografa")
	private int checkedBy = 0;

	public BlockCheck() {
		super();
	}

	public BlockCheck(User cartographer, Block block) {
		super();
		this.cartographer = cartographer;
		this.block = block;
	}

	public User getCartographer() {
		return cartographer;
	}

	public void setCartographer(User cartographer) {
		this.cartographer = cartographer;
	}

	public Block getBlock() {
		return block;
	}

	public void setBlock(Block block) {
		this.block = block;
	}

	public int getCheckedBy() {
		return checkedBy;
	}

	public void addCheckedBy(int checkedBy) {
		this.checkedBy += checkedBy;
	}

	public long getId() {
		return id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((block == null) ? 0 : block.hashCode());
		result = prime * result + ((cartographer == null) ? 0 : cartographer.hashCode());
		result = prime * result + checkedBy;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BlockCheck other = (BlockCheck) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
	
}
