package SultansOfSwing.HumanitarnoKartografiranje.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.vividsolutions.jts.geom.Polygon;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;

@Entity(name="Građevina")
public class Building {
	
	@Column(name="IDGrađevina")
	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	private long id;
	
	@Column(name="nazivGrađevina", unique = true)
	private String name;
	
	@Column(name="statusGrađevina")
	private boolean isSearched;
	
	@Column(name="podrucjeGrađevina", nullable=false)
	private Polygon area;
	
	@ManyToOne
	@JoinColumn(nullable=false)
	private Block block;

	public Building() {
		super();
	}

	public Building(String name, List<List<Double>> area, Block block) {
		super();
		this.name = name;
		this.block = block;
		this.isSearched = false;
		
		Coordinate[] points = new Coordinate[area.size()];
		int i = 0;
		for (List<Double> list : area) {

			if(list.size() != 2) throw new IllegalArgumentException("Invalid point.");
			points[i] = new Coordinate(list.get(0), list.get(1));
			i++;

		}
		GeometryFactory gf = new GeometryFactory();
		this.area = gf.createPolygon(points);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isSearched() {
		return isSearched;
	}

	public void setSearched(boolean isSearched) {
		this.isSearched = isSearched;
	}

	public Polygon getArea() {
		return area;
	}

	public void setArea(Polygon area) {
		this.area = area;
	}

	public Block getBlock() {
		return block;
	}

	public void setBlock(Block block) {
		this.block = block;
	}

	public long getId() {
		return id;
	}	

}
