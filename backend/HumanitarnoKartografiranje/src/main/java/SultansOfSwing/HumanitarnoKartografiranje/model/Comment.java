package SultansOfSwing.HumanitarnoKartografiranje.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="Komentar")
public class Comment {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	@ManyToOne
	@JoinColumn(nullable=false)
	private MissingPerson missingPerson;
	
	@Column(name="tekst", nullable=false)
	private String text;

	public Comment() {
		super();
	}

	public Comment(String text, MissingPerson missingPerson) {
		super();
		this.text = text;
		this.missingPerson = missingPerson;
	}

	public MissingPerson getMissingPerson() {
		return missingPerson;
	}

	public void setMissingPerson(MissingPerson missingPerson) {
		this.missingPerson = missingPerson;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public long getId() {
		return id;
	}
	
}
