package SultansOfSwing.HumanitarnoKartografiranje.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name="StatistikaGrađevine")
public class BuildingStatistics {

	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	@Column(nullable=false, unique= true)
	private String date;
	
	@Column
	private int numberOfSearched;
	
	@Column
	private int numberOfUnsearched;

	public BuildingStatistics() {
		super();
		numberOfSearched = 0;
		numberOfUnsearched = 0;
	}

	public BuildingStatistics(String date, int numberOfSearched, int numberOfUnsearched) {
		super();
		this.date = date;
		this.numberOfSearched = numberOfSearched;
		this.numberOfUnsearched = numberOfUnsearched;
	}

	public long getId() {
		return id;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public int getNumberOfSearched() {
		return numberOfSearched;
	}

	public void addNumberOfSearched(int numberOfSearched) {
		this.numberOfSearched += numberOfSearched;
	}

	public int getNumberOfUnsearched() {
		return numberOfUnsearched;
	}

	public void addNumberOfUnsearched(int numberOfUnsearched) {
		this.numberOfUnsearched += numberOfUnsearched;
	}

}
