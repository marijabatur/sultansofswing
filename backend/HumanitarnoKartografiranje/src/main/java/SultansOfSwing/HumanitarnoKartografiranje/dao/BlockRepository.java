package SultansOfSwing.HumanitarnoKartografiranje.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import SultansOfSwing.HumanitarnoKartografiranje.model.Block;

@Repository
public interface BlockRepository extends JpaRepository<Block, Long> {

	Block findByName(String blockName);

}
