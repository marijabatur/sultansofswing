package SultansOfSwing.HumanitarnoKartografiranje.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import SultansOfSwing.HumanitarnoKartografiranje.model.Region;

@Repository
public interface RegionRepository extends JpaRepository<Region, Long> {
	
	public Region findByName(String name);
}
