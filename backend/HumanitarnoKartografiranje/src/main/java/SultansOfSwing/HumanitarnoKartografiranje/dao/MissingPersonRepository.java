package SultansOfSwing.HumanitarnoKartografiranje.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import SultansOfSwing.HumanitarnoKartografiranje.model.MissingPerson;

@Repository
public interface MissingPersonRepository extends JpaRepository<MissingPerson, Long> {

}
