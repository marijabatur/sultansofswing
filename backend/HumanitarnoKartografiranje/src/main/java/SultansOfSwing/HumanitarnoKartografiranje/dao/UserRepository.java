package SultansOfSwing.HumanitarnoKartografiranje.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import SultansOfSwing.HumanitarnoKartografiranje.model.User;

@Repository
@Transactional
public interface UserRepository extends JpaRepository<User, Long> {
	
	@Query("SELECT u FROM Korisnik u WHERE u.userName = :n")
	public User findByUserName(@Param("n") String userName);
	
	@Query("SELECT u FROM Korisnik u WHERE u.registered = false")
	public List<User> findAllUnregistered();
	
	@Query("UPDATE Korisnik u set u.registered = true WHERE u.id = :n")
	@Modifying
	public void approveRegistration(@Param("n") Long id);

	@Query("SELECT u FROM Korisnik u WHERE u.registered = true")
	public List<User> findAllRegistered();

}
