package SultansOfSwing.HumanitarnoKartografiranje.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import SultansOfSwing.HumanitarnoKartografiranje.model.BuildingStatistics;

@Repository
public interface BuildingStatisticsRepository extends JpaRepository<BuildingStatistics, Long> {

	BuildingStatistics findByDate(String date);

}
