package SultansOfSwing.HumanitarnoKartografiranje.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import SultansOfSwing.HumanitarnoKartografiranje.model.Building;

@Repository
public interface BuildingRepository extends JpaRepository<Building, Long> {

	Building findByName(String buildingName);

}
