package SultansOfSwing.HumanitarnoKartografiranje.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import SultansOfSwing.HumanitarnoKartografiranje.model.MissingPersonStatistics;

@Repository
public interface MissingPersonStatisticsRepository extends JpaRepository<MissingPersonStatistics, Long>{
	
	public MissingPersonStatistics findByDate(String date);
}
