package SultansOfSwing.HumanitarnoKartografiranje.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import SultansOfSwing.HumanitarnoKartografiranje.DTO.CommentDTO;
import SultansOfSwing.HumanitarnoKartografiranje.DTO.MissingPersonDTO;
import SultansOfSwing.HumanitarnoKartografiranje.DTO.StatisticsDTO;

public interface IMissingPersonService {
	
	public List<MissingPersonDTO> getAllMissingPeople();

	public List<CommentDTO> getComments(Long id);

	public void lockReport(Long id);

	public void deleteReport(Long id);

	public void addComment(Long id, String commentText);

	public void deleteComment(Long idPerson, Long idComment);

	public long reportMissingPerson(MissingPersonDTO missingPerson);

	void addPicture(MultipartFile picture, Long id);

	public List<StatisticsDTO> getRepotedPeopleStatistics();

	public List<StatisticsDTO> getFoundPeopleStatistics();

}
