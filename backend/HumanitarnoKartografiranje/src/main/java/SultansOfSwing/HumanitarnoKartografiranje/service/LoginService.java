package SultansOfSwing.HumanitarnoKartografiranje.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import SultansOfSwing.HumanitarnoKartografiranje.DTO.RegistrationDTO;
import SultansOfSwing.HumanitarnoKartografiranje.dao.UserRepository;
import SultansOfSwing.HumanitarnoKartografiranje.model.User;

@Service
public class LoginService implements LoginServiceInterface {
	
	@Autowired
	private UserRepository userRepository;
	
	@Override
	public RegistrationDTO checkLogin(String userName, String password) {
		
		User user = userRepository.findByUserName(userName);
		
		if (user == null) throw new IllegalArgumentException("User doesen't exist.");
		
		PasswordEncoder encoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
		
		if (!encoder.matches(password, user.getPassword())) throw new IllegalArgumentException("Incorrect password");
		else if (user.isRegistered() == false) throw new IllegalArgumentException("You haven't been approved by administrator");
		
		return new RegistrationDTO(
				user.getId(),
				user.getUserName(),
				user.getPassword(),
				user.getFirstName(),
				user.getLastName(),
				user.getPhoneNumber(),
				user.getEmail(),
				user.getRole(),
				user.getPictureURL()
				);
	}

}
