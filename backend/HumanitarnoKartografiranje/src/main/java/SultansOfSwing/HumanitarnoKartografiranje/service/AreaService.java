package SultansOfSwing.HumanitarnoKartografiranje.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Polygon;

import SultansOfSwing.HumanitarnoKartografiranje.DTO.BlockDTO;
import SultansOfSwing.HumanitarnoKartografiranje.DTO.BuildingDTO;
import SultansOfSwing.HumanitarnoKartografiranje.DTO.RegionDTO;
import SultansOfSwing.HumanitarnoKartografiranje.DTO.StatisticsDTO;
import SultansOfSwing.HumanitarnoKartografiranje.dao.BlockRepository;
import SultansOfSwing.HumanitarnoKartografiranje.dao.BlockStatisticsRepository;
import SultansOfSwing.HumanitarnoKartografiranje.dao.BlockUserRepository;
import SultansOfSwing.HumanitarnoKartografiranje.dao.BuildingRepository;
import SultansOfSwing.HumanitarnoKartografiranje.dao.BuildingStatisticsRepository;
import SultansOfSwing.HumanitarnoKartografiranje.dao.RegionRepository;
import SultansOfSwing.HumanitarnoKartografiranje.dao.UserRepository;
import SultansOfSwing.HumanitarnoKartografiranje.model.Block;
import SultansOfSwing.HumanitarnoKartografiranje.model.BlockCheck;
import SultansOfSwing.HumanitarnoKartografiranje.model.BlockStatistics;
import SultansOfSwing.HumanitarnoKartografiranje.model.BlockStatus;
import SultansOfSwing.HumanitarnoKartografiranje.model.Building;
import SultansOfSwing.HumanitarnoKartografiranje.model.BuildingStatistics;
import SultansOfSwing.HumanitarnoKartografiranje.model.Region;
import SultansOfSwing.HumanitarnoKartografiranje.model.User;

@Service
public class AreaService implements IAreaService {
	
	@Autowired
	private RegionRepository regionRepository;
	
	@Autowired
	private BlockRepository blockRepository;
	
	@Autowired
	private BuildingRepository buildingRepository;
	
	@Autowired
	private BlockStatisticsRepository blockStatisticsRepository;
	
	@Autowired
	private BuildingStatisticsRepository buildingStatisticsRepository;
	
	@Autowired
	private BlockUserRepository blockUserRepository;
	
	@Autowired
	private UserRepository userRepository;

	@Override
	public void defineRegion(RegionDTO region) {
		if(region.getName() == null) throw new IllegalArgumentException("Region name cannot be null!");
		if(region.getArea() == null) throw new IllegalArgumentException("Region area cannot be null!");
		
		Region instance = new Region(region.getName(), region.getArea());
		
		regionRepository.save(instance);
	}

	@Override
	public void defineBlock(BlockDTO block) {
		if(block.getName() == null) throw new IllegalArgumentException("Block name cannot be null!");
		if(block.getRegionName() == null) throw new IllegalArgumentException("Region name cannot be null!");
		if(block.getArea() == null) throw new IllegalArgumentException("Block area cannot be null!");
		
		Region blockRegion = regionRepository.findByName(block.getRegionName());
		if(blockRegion == null) throw new IllegalArgumentException("Unknown region name.");

		Block instance = new Block(block.getName(), block.getArea());
		instance.setRegion(blockRegion);
		
		if(!checkCoordinatesInArea(instance.getArea(), blockRegion.getArea())) {
			throw new IllegalArgumentException(
					"Block " + instance.getName() + " is not within region " + block.getRegionName()
					);
		}
		
		blockRepository.save(instance);
		
		blockRegion.getBlocks().add(instance);
		regionRepository.save(blockRegion);
	}

	@Override
	public List<StatisticsDTO> getBlockStatistics() {
		List<BlockStatistics> blocks = blockStatisticsRepository.findAll();
		List<StatisticsDTO> statistics = new ArrayList<>();
		
		for (BlockStatistics block : blocks) {
			statistics.add(new StatisticsDTO(block.getDate(), block.getNumberOfFinished()));
		}
		return statistics;
	}

	@Override
	public List<StatisticsDTO> getUnsearchedBuildingStatistics() {
		List<BuildingStatistics> buildings = buildingStatisticsRepository.findAll();
		List<StatisticsDTO> statistics = new ArrayList<>();
		
		for (BuildingStatistics building : buildings) {
			statistics.add(new StatisticsDTO(building.getDate(), building.getNumberOfUnsearched()));
		}
		return statistics;
	}

	@Override
	public List<StatisticsDTO> getSearchedBuildingStatistics() {
		List<BuildingStatistics> buildings = buildingStatisticsRepository.findAll();
		List<StatisticsDTO> statistics = new ArrayList<>();
		
		for (BuildingStatistics building : buildings) {
			statistics.add(new StatisticsDTO(building.getDate(), building.getNumberOfSearched()));
		}
		return statistics;
	}

	@Override
	public void addBuilding(BuildingDTO building) {
		if(building.getName() == null) throw new IllegalArgumentException("Building name cannot be null!");
		if(building.getBlockName() == null) throw new IllegalArgumentException("Block name cannot be null!");
		if(building.getArea() == null) throw new IllegalArgumentException("Building area cannot be null!");
		
		Block buildingBlock = blockRepository.findByName(building.getBlockName());
		if(buildingBlock == null) throw new IllegalArgumentException("The given block name doesn't exist!");

		Building instance = new Building(building.getName(), building.getArea(), buildingBlock);
		
		if(!checkCoordinatesInArea(instance.getArea(), buildingBlock.getArea())) {
			throw new IllegalArgumentException(
					"Building " + instance.getName() + " is not within block " + building.getBlockName()
					);
		}
		
		buildingRepository.save(instance);
		
		buildingBlock.getBuildings().add(instance);
		blockRepository.save(buildingBlock);
		
		Date today = new Date();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		
		String date = format.format(today);
		
		BuildingStatistics statistics = buildingStatisticsRepository.findByDate(date);
		
		if(statistics == null) {
			statistics = new BuildingStatistics(date, 0, 1);
		} else {
			statistics.addNumberOfUnsearched(1);;
		}
		
		buildingStatisticsRepository.save(statistics);
		
	}

	private boolean checkCoordinatesInArea(Polygon innerArea, Polygon outerArea) {
	//Crossing numbers algorithm (CNA)
		
		for (Coordinate point : innerArea.getCoordinates()) {
			
			int numberOfCrossings = 0;
			Coordinate[] outerCoordinates = outerArea.getCoordinates();
			
			for (int i = 0; i < outerCoordinates.length - 1; i++) {
				
				if((outerCoordinates[i].x < point.x && point.x < outerCoordinates[i+1].x) ||
						(outerCoordinates[i].x > point.x && point.x > outerCoordinates[i+1].x)) {
					double t = (point.x - outerCoordinates[i+1].x) / (outerCoordinates[i].x - outerCoordinates[i+1].x);
					double cy = t*outerCoordinates[i].y + (1-t)*outerCoordinates[i+1].y;
					
					if(Math.abs(point.y - cy) < 10E-10) {
						numberOfCrossings = 0;
						break;
					}
					
					if(point.y > cy) numberOfCrossings++;
				}
				
				if(Math.abs(outerCoordinates[i].x - point.x) < 10E-10 && outerCoordinates[i].y <= point.y) {
					
					if(Math.abs(outerCoordinates[i].y - point.y) < 10E-10) {
						numberOfCrossings = 0;
						break;
					}
					
					if(Math.abs(outerCoordinates[i+1].x - point.x) < 10E-10) {
						if((outerCoordinates[i].y <= point.y && point.y <= outerCoordinates[i+1].y) ||
								outerCoordinates[i].y >= point.y && point.y >= outerCoordinates[i+1].y){
							numberOfCrossings = 0;
							break;
						}
					} else if(outerCoordinates[i+1].x > point.x) numberOfCrossings++;
					
					if(i != 0 && outerCoordinates[i-1].x > point.x) numberOfCrossings++;
				}
			}
			
			if(numberOfCrossings%2 == 0) return false;
			
		}
		return true;
		
	}

	@Override
	public BlockDTO getBlock(String username) {
		User user = userRepository.findByUserName(username);		
		if(user == null) throw new IllegalArgumentException("User with username " + username + " doesn't exist!");
		if(!user.getRole().equals("kartograf")) throw new IllegalArgumentException("User with username " + username + " is not a cartographer!");
		
		List<BlockCheck> blockChecks = user.getBlocks();
		
		BlockCheck blockCheck = null;
		
		for (BlockCheck block : blockChecks) {
			if(block.getBlock().getStatus() == BlockStatus.AKTIVNO) {
				blockCheck = block;
			}
		}
		
		if(blockCheck == null)
			throw new IllegalArgumentException("You have no active blocks.");
		
		Block block = blockCheck.getBlock();
		
		return new BlockDTO(block.getName(), getPolygonAsList(block.getArea()), block.getStatus().toString());
	}

	@Override
	public void setBlock(String username, String blockName) {
		User user = userRepository.findByUserName(username);	
		if(user == null) throw new IllegalArgumentException("User with username " + username + " doesn't exist!");
		if(!user.getRole().equals("kartograf")) throw new IllegalArgumentException("User with username " + username + " is not a cartographer!");
		
		Block block = blockRepository.findByName(blockName);
		if(block == null) throw new IllegalArgumentException("Block with the given name doesn't exist!");		
		if(block.getStatus() != BlockStatus.NEZAPOČETO) throw new IllegalArgumentException("The block is already being mapped!");
		
		block.setStatus(BlockStatus.AKTIVNO);
		
		List<BlockCheck> blockChecks = user.getBlocks();
		
		for (BlockCheck blockCheck : blockChecks) {
			if(blockCheck.getBlock().getStatus() == BlockStatus.AKTIVNO)
				throw new IllegalArgumentException("You already have an active block.");
		}
		
		blockRepository.save(block);

		BlockCheck instance = new BlockCheck(user, block);
		blockChecks.add(instance);
		userRepository.save(user);
		
		blockUserRepository.save(instance);
	}

	@Override
	public void approveBlock(String username, String blockName) {
		User user = userRepository.findByUserName(username);
		if(user == null) throw new IllegalArgumentException("User with username " + username + " doesn't exist!");
		if(!user.getRole().equals("kartograf")) throw new IllegalArgumentException("User with username " + username + " is not a cartographer!");
		
		Block block = blockRepository.findByName(blockName);
		if(block == null) throw new IllegalArgumentException("Block with the given name doesn't exist!");
		
		if(block.getStatus() != BlockStatus.PROVJERA) throw new IllegalArgumentException("The block cannot be approved!");
		
		BlockCheck blockCheck = block.getAssignedCartographer();
		
		blockCheck.addCheckedBy(1);
		
		if(blockCheck.getCheckedBy() == 2) {
			blockCheck.getBlock().setStatus(BlockStatus.ZAVRŠENO);
		}
		
		blockRepository.save(blockCheck.getBlock());
		blockUserRepository.save(blockCheck);
		
	}

	@Override
	public void disapproveBlock(String username, String blockName) {
		User user = userRepository.findByUserName(username);
		if(user == null) throw new IllegalArgumentException("User with username " + username + " doesn't exist!");
		if(!user.getRole().equals("kartograf")) throw new IllegalArgumentException("User with username " + username + " is not a cartographer!");
		
		Block block = blockRepository.findByName(blockName);
		if(block == null) throw new IllegalArgumentException("Block with the given name doesn't exist!");
		if(block.getStatus() != BlockStatus.PROVJERA) throw new IllegalArgumentException("The block cannot be dissapproved!");	
		
		List<BlockCheck> blockChecks = user.getBlocks();
		
		for (BlockCheck blockCheck : blockChecks) {
			if(blockCheck.getBlock().getStatus() == BlockStatus.AKTIVNO)
				throw new IllegalArgumentException("You already have an active block.");
		}
		
		BlockCheck blockCheck = block.getAssignedCartographer();
		blockCheck.getCartographer().getBlocks().remove(blockCheck);
		
		blockCheck.setCartographer(user);
		user.getBlocks().add(blockCheck);
		
		block.setStatus(BlockStatus.AKTIVNO);
		blockCheck.addCheckedBy(-blockCheck.getCheckedBy());
		block.setBuildings(new ArrayList<>());
		
		
		blockRepository.save(block);
		userRepository.save(user);
		
		blockUserRepository.save(blockCheck);
		
	}

	@Override
	public void finishBlock(String username, String blockName) {
		User user = userRepository.findByUserName(username);
		if(user == null) throw new IllegalArgumentException("User with username " + username + " doesn't exist!");
		if(!user.getRole().equals("kartograf")) throw new IllegalArgumentException("User with username " + username + " is not a cartographer!");
		
		Block block = blockRepository.findByName(blockName);
		if(block == null) throw new IllegalArgumentException("Block with the given name doesn't exist!");
		if(block.getStatus() != BlockStatus.AKTIVNO) throw new IllegalArgumentException("The block cannot be finished because it isn't active!");	
		
		BlockCheck bc = block.getAssignedCartographer();
		if(bc == null || bc.getCartographer().getId() != user.getId()) throw new IllegalArgumentException("This block doesn't belong to you!");
		
		block.setStatus(BlockStatus.PROVJERA);
		blockRepository.save(block);
		
	}
	
	private List<List<Double>> getPolygonAsList(Polygon polygon) {
		List<List<Double>> area = new ArrayList<>();
		Coordinate[] coordinates = polygon.getCoordinates();
		
		for (int i = 0; i < coordinates.length; i++) {
			List<Double> point = new ArrayList<>();
			point.add(coordinates[i].x);
			point.add(coordinates[i].y);
			area.add(point);
		}
		return area;
	}

	@Override
	public List<BuildingDTO> getBuildings(String blockName) {
		Block block = blockRepository.findByName(blockName);
		if(block == null) throw new IllegalArgumentException("Block with the given name doesn't exist!");
		
		List<Building> buildings = block.getBuildings();
		List<BuildingDTO> buildingDTOs = new ArrayList<>();
		
		for (Building building : buildings) {
			BuildingDTO bDTO = new BuildingDTO(
					building.getName(),
					blockName,
					getPolygonAsList(building.getArea())
					);
			buildingDTOs.add(bDTO);
		}
		
		return buildingDTOs;
	}

	@Override
	public List<BlockDTO> getBlocks() {
		List<BlockDTO> blocks = new ArrayList<>();
		
		for (Block block : blockRepository.findAll()) {
			blocks.add(new BlockDTO(block.getName(), getPolygonAsList(block.getArea()), block.getStatus().toString()));
		}
		return blocks;
	}

	@Override
	public List<RegionDTO> getRegions() {
		List<RegionDTO> regions = new ArrayList<>();
		
		for (Region region : regionRepository.findAll()) {
			regions.add(new RegionDTO(region.getName(), getPolygonAsList(region.getArea())));
		}
		return regions;
	}
	
	@Override
	public List<BlockDTO> getFreeBlocks() {
		List<BlockDTO> blocks = new ArrayList<>();
		
		for (Block block : blockRepository.findAll()) {
			if(block.getStatus() == BlockStatus.NEZAPOČETO) {
				blocks.add(new BlockDTO(block.getName(), getPolygonAsList(block.getArea()), block.getStatus().toString()));
			}
		}
		return blocks;
	}

	@Override
	public List<BuildingDTO> getUnsearchedBuildings() {
		List<BuildingDTO> buildingDTOs = new ArrayList<>();
		List<Building> buildings = buildingRepository.findAll();
		
		for (Building building : buildings) {
			if(!building.isSearched()) {
				
				BuildingDTO bDTO = new BuildingDTO(
						building.getName(),
						null,
						getPolygonAsList(building.getArea())
						);
				buildingDTOs.add(bDTO);
			}
		}
		
		return buildingDTOs;
	}

	@Override
	public void setSearchedBuilding(String buildingName) {
		Building building = buildingRepository.findByName(buildingName);
		if(building == null) throw new IllegalArgumentException("Building with name " + buildingName + " doesn't exist!");
		
		building.setSearched(true);
		buildingRepository.save(building);
	}

	@Override
	public List<BlockDTO> getCheckBlocks() {
		List<BlockDTO> blocks = new ArrayList<>();
		
		for (Block block : blockRepository.findAll()) {
			if(block.getStatus() == BlockStatus.PROVJERA) {
				blocks.add(new BlockDTO(block.getName(), getPolygonAsList(block.getArea()), block.getStatus().toString()));
			}
		}
		return blocks;
	}

}
