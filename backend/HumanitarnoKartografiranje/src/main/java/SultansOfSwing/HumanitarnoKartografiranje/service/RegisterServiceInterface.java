package SultansOfSwing.HumanitarnoKartografiranje.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import SultansOfSwing.HumanitarnoKartografiranje.DTO.RegistrationDTO;
import SultansOfSwing.HumanitarnoKartografiranje.model.User;

@Service
public interface RegisterServiceInterface {
	
	public User registerUser(RegistrationDTO user);
	
	public void acceptUser(User user);

	public List<User> viewRegistrationRequests();
	
	public void declineUser(User user);

	public void addPicture(MultipartFile picture, String username);

	public List<User> listAllUsers();

	public void changeRole(String username, String role);

	void changeUserDetail(String username, String password, String firstName, String lastName, String email,
			String phoneNumber);
}
