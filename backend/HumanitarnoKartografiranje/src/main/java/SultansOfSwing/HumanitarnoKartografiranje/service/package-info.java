/**
 * This package contains all the services responsible for executing the application logic.
 *
 */
package SultansOfSwing.HumanitarnoKartografiranje.service;