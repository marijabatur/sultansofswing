package SultansOfSwing.HumanitarnoKartografiranje.service;

import java.util.List;

import SultansOfSwing.HumanitarnoKartografiranje.DTO.BlockDTO;
import SultansOfSwing.HumanitarnoKartografiranje.DTO.BuildingDTO;
import SultansOfSwing.HumanitarnoKartografiranje.DTO.RegionDTO;
import SultansOfSwing.HumanitarnoKartografiranje.DTO.StatisticsDTO;

public interface IAreaService {

	void defineRegion(RegionDTO region);

	void defineBlock(BlockDTO block);

	List<StatisticsDTO> getBlockStatistics();

	List<StatisticsDTO> getUnsearchedBuildingStatistics();

	List<StatisticsDTO> getSearchedBuildingStatistics();

	void addBuilding(BuildingDTO building);

	BlockDTO getBlock(String username);

	void setBlock(String username, String blockName);

	void approveBlock(String username, String blockName);

	void disapproveBlock(String username, String blockName);

	void finishBlock(String username, String blockName);

	List<BuildingDTO> getBuildings(String blockName);

	List<BlockDTO>  getBlocks();

	List<RegionDTO>  getRegions();

	List<BlockDTO> getFreeBlocks();

	List<BuildingDTO> getUnsearchedBuildings();

	void setSearchedBuilding(String buildingName);

	List<BlockDTO> getCheckBlocks();
	

}
