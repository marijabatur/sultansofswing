package SultansOfSwing.HumanitarnoKartografiranje.service;

import org.springframework.stereotype.Service;

import SultansOfSwing.HumanitarnoKartografiranje.DTO.RegistrationDTO;

@Service
public interface LoginServiceInterface {
	
	public RegistrationDTO checkLogin(String userName, String password);
	
}
