package SultansOfSwing.HumanitarnoKartografiranje.service;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.google.cloud.WriteChannel;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;

import SultansOfSwing.HumanitarnoKartografiranje.DTO.RegistrationDTO;
import SultansOfSwing.HumanitarnoKartografiranje.dao.UserRepository;
import SultansOfSwing.HumanitarnoKartografiranje.model.User;

@Service
public class RegisterService implements RegisterServiceInterface {
	
	private static final String BUCKET = "humanitarnokartografiranjeopp";
	private static final String STORAGE = "https://storage.cloud.google.com";
	
	@Autowired
	private Storage storage;
	
	@Autowired
	private UserRepository userRepository;
	
	@Override
	public User registerUser(RegistrationDTO userDTO) {
		checkUsername(userDTO.getUserName());
		checkPassword(userDTO.getPassword());
		checkRole(userDTO.getRole());
		
		if(!checkPresence(userDTO.getFirstName())) throw new IllegalArgumentException("First name cannot be blank.");
		if(!checkPresence(userDTO.getLastName())) throw new IllegalArgumentException("Last name cannot be blank.");
		if(!checkPresence(userDTO.getPhoneNumber())) throw new IllegalArgumentException("Last name cannot be blank.");
		if(!checkPresence(userDTO.getEmail())) throw new IllegalArgumentException("Last name cannot be blank.");
		
		if (!validate(userDTO.getEmail())) throw new IllegalArgumentException("E-mail isn't valid");  
		
		User user = new User(
				userDTO.getUserName(),
				PasswordEncoderFactories.createDelegatingPasswordEncoder().encode(userDTO.getPassword()),
				userDTO.getFirstName(),
				userDTO.getLastName(),
				null,
				userDTO.getPhoneNumber(),
				userDTO.getEmail(),
				userDTO.getRole());
		try {
			userRepository.save(user);
		} catch (Exception e) {
			throw new IllegalArgumentException(e.getMessage());
		}

		return user;
	}

	
	private void storePicture(MultipartFile picture, String username) {

		BlobId blobId = BlobId.of(BUCKET, username);
		
		BlobInfo blobInfo = BlobInfo.newBuilder(blobId).setContentType("image/jpg").build();
		
		try (WriteChannel writer = storage.writer(blobInfo)) {
				
			try {
				writer.write(ByteBuffer.wrap(picture.getBytes()));
			} catch (Exception ex) {
				System.err.println(ex.getMessage());
			}
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
	}


	public void checkRole(String role) {
		if(role == null) throw new IllegalArgumentException("Role must be provided");
		if (!(role.equals("spasioc") || role.equals("voditelj") || role.equals("kartograf"))) throw new IllegalArgumentException("Role isn't valid");
		
	}


	public boolean checkPresence(String entry) {
		return (entry != null && !entry.trim().isEmpty());
	}


	public void checkPassword(String password) {
		if(password == null) throw new IllegalArgumentException("Password must be provided");
		if (password.length() < 6 || password.length() > 32) throw new IllegalArgumentException("Password must be between 6 and 32 characters.");  	
	}


	public void checkUsername(String username) {
		if(username == null) throw new IllegalArgumentException("Username must be provided");
		if (username.length() < 4 || username.length() > 16) throw new IllegalArgumentException("Username must be between 4 and 16 characters."); 
	}


	@Override  
	public List<User> viewRegistrationRequests() {
		return userRepository.findAllUnregistered();
	} 


	@Override
	public void acceptUser(User user) {
		userRepository.approveRegistration(user.getId());

	}

	@Override
	public void declineUser(User user) {
		userRepository.delete(user);

	}
	
	private static final Pattern VALID_EMAIL_ADDRESS_REGEX = 
		    Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

	private static boolean validate(String emailStr) {
		        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr);
		        return matcher.find();
	}

	@Override
	public void addPicture(MultipartFile picture, String username) {
		storePicture(picture, username);
		User user = userRepository.findByUserName(username);
		if(user == null) throw new IllegalArgumentException("Invalid username");
		
		user.setPictureURL(STORAGE + "/" + BUCKET + "/" + username);
		userRepository.save(user);
	}


	@Override
	public List<User> listAllUsers() {
		return userRepository.findAllRegistered();
	}


	@Override
	public void changeRole(String username, String role) {
		User user = userRepository.findByUserName(username);
		if(user == null) throw new IllegalArgumentException("Invalid username");
		
		checkRole(role);
		
		user.setRole(role);
		userRepository.save(user);
	}
	

	public RegistrationDTO getUserDetails(String username) {
		User user = userRepository.findByUserName(username);
		if(user == null) throw new IllegalArgumentException("Invalid username");
		return new RegistrationDTO(
				user.getId(),
				user.getUserName(),
				user.getPassword(),
				user.getFirstName(),
				user.getLastName(),
				user.getPhoneNumber(),
				user.getEmail(),
				user.getRole(),
				user.getPictureURL()
				);
	}


	@Override
	public void changeUserDetail(String username, String password, String firstName, String lastName, String email,
			String phoneNumber) {
		checkUsername(username);

		User user = userRepository.findByUserName(username);
		if(user == null) throw new IllegalArgumentException("Invalid username");

		if(password != null) {
			checkPassword(password);
			user.setPassword(PasswordEncoderFactories.createDelegatingPasswordEncoder().encode(password));
		};	

		if(checkPresence(firstName)) {
			user.setFirstName(firstName);
		}
		
		if(checkPresence(lastName)) {
			user.setLastName(lastName);
		}
		
		if(checkPresence(phoneNumber)) {
			user.setPhoneNumber(phoneNumber);
		}
		
		if(checkPresence(email)) {
			if (!validate(email)) throw new IllegalArgumentException("E-mail isn't valid");
			user.setEmail(email);
		}
		
		userRepository.save(user);
	}
}
