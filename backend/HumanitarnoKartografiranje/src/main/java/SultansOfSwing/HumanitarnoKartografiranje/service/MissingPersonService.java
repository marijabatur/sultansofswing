package SultansOfSwing.HumanitarnoKartografiranje.service;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.google.cloud.WriteChannel;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;

import SultansOfSwing.HumanitarnoKartografiranje.DTO.CommentDTO;
import SultansOfSwing.HumanitarnoKartografiranje.DTO.MissingPersonDTO;
import SultansOfSwing.HumanitarnoKartografiranje.DTO.StatisticsDTO;
import SultansOfSwing.HumanitarnoKartografiranje.dao.CommentRepository;
import SultansOfSwing.HumanitarnoKartografiranje.dao.MissingPersonRepository;
import SultansOfSwing.HumanitarnoKartografiranje.dao.MissingPersonStatisticsRepository;
import SultansOfSwing.HumanitarnoKartografiranje.model.Comment;
import SultansOfSwing.HumanitarnoKartografiranje.model.MissingPerson;
import SultansOfSwing.HumanitarnoKartografiranje.model.MissingPersonStatistics;

@Service
public class MissingPersonService implements IMissingPersonService {
	
	private static final String BUCKET = "humanitarnokartografiranjeopp";
	private static final String STORAGE = "https://storage.cloud.google.com";
	
	@Autowired
	private Storage storage;
	
	@Autowired
	private MissingPersonRepository missingPersonRepository;
	
	@Autowired
	private CommentRepository commentRepository;
	
	@Autowired
	private MissingPersonStatisticsRepository statisticsRepository;

	@Override
	public List<MissingPersonDTO> getAllMissingPeople() {
		List<MissingPerson> missingPeople = missingPersonRepository.findAll();
		List<MissingPersonDTO> missingPeopleDto = new ArrayList<>();
		
		for (MissingPerson missingPerson : missingPeople) {
			if(!missingPerson.isFound()) {				
				missingPeopleDto.add(
						new MissingPersonDTO(
								missingPerson.getId(),
								missingPerson.getFirstName(),
								missingPerson.getLastName(),
								missingPerson.getDescription(),
								missingPerson.isFound(),
								missingPerson.getPictureURL()
								));
			}
		}
		
		return missingPeopleDto;
	}

	@Override
	public List<CommentDTO> getComments(Long id) {
		if(id == null) throw new IllegalArgumentException("Id must be provided!");
		
		Optional<MissingPerson> missingPerson = missingPersonRepository.findById(id);
		if(missingPerson.isEmpty()) throw new IllegalArgumentException("Missing person with id " + id + " doesn't exist!");
		
		MissingPerson person = missingPerson.get();
		
		List<CommentDTO> comments = new ArrayList<>();
		 for (Comment comment : person.getComments()) {
			 comments.add(new CommentDTO(comment.getText(), comment.getId()));
		};
		return comments;
	}

	@Override
	public void lockReport(Long id) {
		if(id == null) throw new IllegalArgumentException("Id must be provided!");
		
		Optional<MissingPerson> missingPerson = missingPersonRepository.findById(id);
		if(missingPerson.isEmpty()) throw new IllegalArgumentException("Missing person with id " + id + " doesn't exist!");
		
		MissingPerson person = missingPerson.get();
		
		person.setFound(true);
		missingPersonRepository.save(person);
		
		Date today = new Date();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		
		String date = format.format(today);
		
		MissingPersonStatistics statistics = statisticsRepository.findByDate(date);
		
		if(statistics == null) {
			statistics = new MissingPersonStatistics(date, 0, 1);
		} else {
			statistics.setNumberOfFound(statistics.getNumberOfFound() + 1);
		}
		
		statisticsRepository.save(statistics);
		
	}

	@Override
	public void deleteReport(Long id) {
		missingPersonRepository.deleteById(id);
	}

	@Override
	public void addComment(Long id, String commentText) { 
		if(id == null) throw new IllegalArgumentException("Id must be provided!");
		if(commentText == null) throw new IllegalArgumentException("Comment text must be provided!");
		
		Optional<MissingPerson> missingPerson = missingPersonRepository.findById(id);
		if(missingPerson.isEmpty()) throw new IllegalArgumentException("Missing person with id " + id + " doesn't exist!");
		
		MissingPerson person = missingPerson.get();
		
		Comment comment = new Comment(commentText, person);
		commentRepository.save(comment);
		
		person.getComments().add(comment);
		missingPersonRepository.save(person);
	}

	@Override
	public void deleteComment(Long idPerson, Long idComment) {
		if(idPerson == null) throw new IllegalArgumentException("Missing person id must be provided!");
		if(idComment == null) throw new IllegalArgumentException("Comment id must be provided!");
		
		Optional<MissingPerson> missingPerson = missingPersonRepository.findById(idPerson);
		if(missingPerson.isEmpty()) throw new IllegalArgumentException("Missing person with id " + idPerson + " doesn't exist!");
		
		MissingPerson person = missingPerson.get();
		
		Iterator<Comment> it = person.getComments().iterator();
		while (it.hasNext()) {
			Comment comment = it.next();
			
			if(idComment == comment.getId()) {
				it.remove();
			}
		}
		
		missingPersonRepository.save(person);
	}

	@Override
	public long reportMissingPerson(MissingPersonDTO missingPerson) {
		if(missingPerson == null) throw new IllegalArgumentException("Missing person cannot be null!");
		
		MissingPerson person = new MissingPerson(
				missingPerson.getFirstName(),
				missingPerson.getLastName(),
				missingPerson.getDescription(),
				null);
				
		missingPersonRepository.save(person);	
		
		Date today = new Date();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		
		String date = format.format(today);
		
		MissingPersonStatistics statistics = statisticsRepository.findByDate(date);
		
		if(statistics == null) {
			statistics = new MissingPersonStatistics(date, 1, 0);
		} else {
			statistics.setNumberOfReported(statistics.getNumberOfReported() + 1);
		}
		
		statisticsRepository.save(statistics);
		
		return person.getId();
	}
	
	@Override
	public void addPicture(MultipartFile picture, Long id) {
		storePicture(picture, id);
		
		Optional<MissingPerson> missingPerson = missingPersonRepository.findById(id);
		if(missingPerson.isEmpty()) throw new IllegalArgumentException("Invalid missing person id.");
		
		MissingPerson person = missingPerson.get();
		
		person.setPictureURL(STORAGE + "/" + BUCKET + "/" + id);
		missingPersonRepository.save(person);
	}

	private void storePicture(MultipartFile picture, long id) {

		BlobId blobId = BlobId.of(BUCKET, String.valueOf(id));
		
		BlobInfo blobInfo = BlobInfo.newBuilder(blobId).setContentType("image/jpg").build();
		
		try (WriteChannel writer = storage.writer(blobInfo)) {
				
			try {
				writer.write(ByteBuffer.wrap(picture.getBytes()));
			} catch (Exception ex) {
				System.err.println(ex.getMessage());
			}
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
	}

	@Override
	public List<StatisticsDTO> getRepotedPeopleStatistics() {
		List<MissingPersonStatistics> statistics = statisticsRepository.findAll();
		List<StatisticsDTO> data = new ArrayList<>();
		
		for (MissingPersonStatistics missingPersonStatistics : statistics) {
			data.add(
					new StatisticsDTO(
							missingPersonStatistics.getDate(),
							missingPersonStatistics.getNumberOfReported()
							)
					);
		}
		return data;
	}

	@Override
	public List<StatisticsDTO> getFoundPeopleStatistics() {
		List<MissingPersonStatistics> statistics = statisticsRepository.findAll();
		List<StatisticsDTO> data = new ArrayList<>();
		
		for (MissingPersonStatistics missingPersonStatistics : statistics) {
			data.add(
					new StatisticsDTO(
							missingPersonStatistics.getDate(),
							missingPersonStatistics.getNumberOfFound()
							)
					);
		}
		return data;
	}

}
