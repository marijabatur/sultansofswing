package SultansOfSwing.HumanitarnoKartografiranje.service;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import SultansOfSwing.HumanitarnoKartografiranje.dao.UserRepository;
import SultansOfSwing.HumanitarnoKartografiranje.model.User;


@Service
public class UserMainDetailsService implements UserDetailsService {
    private UserRepository userRepository;

    public UserMainDetailsService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }


    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        User user = this.userRepository.findByUserName(s);  
        UserMain userMain = new UserMain(user);

        return userMain;
    }
}
