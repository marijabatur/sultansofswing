package SultansOfSwing.HumanitarnoKartografiranje.DTO;

import java.util.List;

public class BuildingDTO {

	private String name;
	private String blockName;
	private  List<List<Double>> area;
	
	public BuildingDTO() {
		super();
	}

	public BuildingDTO(String name, String blockName, List<List<Double>> area) {
		super();
		this.name = name;
		this.blockName = blockName;
		this.area = area;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBlockName() {
		return blockName;
	}

	public void setBlockName(String blockName) {
		this.blockName = blockName;
	}

	public List<List<Double>> getArea() {
		return area;
	}

	public void setArea(List<List<Double>> area) {
		this.area = area;
	}
	
}
