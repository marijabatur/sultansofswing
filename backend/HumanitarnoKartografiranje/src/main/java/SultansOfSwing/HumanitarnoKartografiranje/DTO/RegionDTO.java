package SultansOfSwing.HumanitarnoKartografiranje.DTO;

import java.util.List;

public class RegionDTO {
	
	private String name;
	private  List<List<Double>> area;
	
	public RegionDTO() {
		super();
	}
	
	public RegionDTO(String name, List<List<Double>> area) {
		super();
		this.name = name;
		this.area = area;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public List<List<Double>> getArea() {
		return area;
	}

	public void setArea(List<List<Double>> area) {
		this.area = area;
	}

}
