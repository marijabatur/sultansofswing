package SultansOfSwing.HumanitarnoKartografiranje.DTO;

public class CommentDTO {
	
	private String text;
	private Long id;
	
	public CommentDTO() {
		super();

	}
	
	public CommentDTO(String text, Long id) {
		super();
		this.text = text;
		this.id = id;
	}
	
	public String getText() {
		return text;
	}
	
	public void setText(String text) {
		this.text = text;
	}
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

}
