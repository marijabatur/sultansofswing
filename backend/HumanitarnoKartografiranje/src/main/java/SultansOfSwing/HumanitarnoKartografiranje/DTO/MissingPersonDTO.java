package SultansOfSwing.HumanitarnoKartografiranje.DTO;

public class MissingPersonDTO {
	
	private long id;
	
	private String firstName;
	
	private String lastName;
	
	private String description;

	private boolean isFound;
	
	private String pictureURL;

	public String getPictureURL() {
		return pictureURL;
	}

	public void setPictureURL(String pictureURL) {
		this.pictureURL = pictureURL;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isFound() {
		return isFound;
	}

	public void setFound(boolean isFound) {
		this.isFound = isFound;
	}

	public MissingPersonDTO(long id, String firstName, String lastName, String description, boolean isFound, String pictureURL) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.description = description;
		this.isFound = isFound;
		this.pictureURL = pictureURL;
	}

	public MissingPersonDTO() {
		super();
	}
	
}
