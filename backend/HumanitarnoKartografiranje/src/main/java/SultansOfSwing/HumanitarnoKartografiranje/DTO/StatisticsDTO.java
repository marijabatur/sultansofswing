package SultansOfSwing.HumanitarnoKartografiranje.DTO;

public class StatisticsDTO {
	
	private String x;
	private int y;
	
	public StatisticsDTO() {
		super();
	}

	public StatisticsDTO(String x, int y) {
		super();
		this.x = x;
		this.y = y;
	}

	public String getX() {
		return x;
	}

	public void setX(String x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}
	
}
