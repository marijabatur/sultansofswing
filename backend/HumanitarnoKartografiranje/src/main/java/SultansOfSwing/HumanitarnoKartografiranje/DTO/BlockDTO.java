package SultansOfSwing.HumanitarnoKartografiranje.DTO;

import java.util.List;

public class BlockDTO {
	
	private String name;
	private String regionName;
	private  List<List<Double>> area;
	private String status;
	
	public BlockDTO() {
		super();
	}
	
	public BlockDTO(String name, List<List<Double>> area, String status) {
		super();
		this.name = name;
		this.area = area;
		this.status = status;
	}
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public List<List<Double>> getArea() {
		return area;
	}

	public void setArea(List<List<Double>> area) {
		this.area = area;
	}
	
	public String getRegionName() {
		return regionName;
	}

	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}

}
