package SultansOfSwing.HumanitarnoKartografiranje.SeTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SeleniumTest {
	
	@Test
	public void loginTest() {
		System.setProperty("webdriver.chrome.driver", "C:\\selenium webdriver\\chromedriver_win32\\chromedriver.exe");
	    WebDriver driver = new ChromeDriver();
	    driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
	    driver.manage().timeouts().pageLoadTimeout(15, TimeUnit.SECONDS);   

	    driver.get("https://kartografiranje-frontend.herokuapp.com/login");
	    
	    
	    WebElement element = driver.findElement(By.name("korisnickoIme"));
	    element.sendKeys("korisnik123");
	    
	    element = driver.findElement(By.name("lozinka"));
	    element.sendKeys("lozinka12");
	    
	    driver.findElement(By.cssSelector("button[type='submit']")).click();
	    

	    new WebDriverWait(driver, 10)
	            .until(ExpectedConditions.textToBePresentInElement(driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/form/div[3]")),"Incorrect username or password!"));

	    
	    WebElement labelNode = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/form/div[3]")); 
	    String labelNodeText = (String) ((JavascriptExecutor)driver).executeScript("return arguments[0].innerHTML",labelNode);

	    assertEquals("Incorrect username or password!", labelNodeText);
	    	    
	    driver.quit();
	}
	
	@Test
	public void registrationTest() {
	    System.setProperty("webdriver.chrome.driver", "C:\\selenium webdriver\\chromedriver_win32\\chromedriver.exe");
	    WebDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.get("https://kartografiranje-frontend.herokuapp.com/registracija");
		
		WebElement element = driver.findElement(By.name("korisnickoIme"));
		element.sendKeys("korisnik12");
		
		element = driver.findElement(By.name("lozinka"));
		element.sendKeys("password12");
		
		element = driver.findElement(By.name("ime"));
		element.sendKeys("Ivo");
		
		element = driver.findElement(By.name("prezime"));
		element.sendKeys("Ivic");
		
		element = driver.findElement(By.name("brojMobitela"));
		element.sendKeys("099555444");
		
		element = driver.findElement(By.name("email"));
		element.sendKeys("IvoIvic@fer.hr");
		
		driver.findElement(By.cssSelector("#root > div > div > form > div:nth-child(9) > label")).click();
		
		driver.findElement(By.cssSelector("#root > div > div > form > button")).click();
		
		new WebDriverWait(driver, 10)
        .until(ExpectedConditions.textToBePresentInElement(driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/form/div[11]")),"Registracija nije uspjela"));
		
		String message = driver.findElement(By.xpath("/html/body/div/div/div/form/div[11]")).getText();
		
		assertEquals("Registracija nije uspjela", message);
		
		driver.quit();
	}
	
	@Test
	public void loginTestsucces() {
		
		System.setProperty("webdriver.chrome.driver", "C:\\selenium webdriver\\chromedriver_win32\\chromedriver.exe");
	    WebDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.get("https://kartografiranje-frontend.herokuapp.com/login");
		
		WebElement element = driver.findElement(By.name("korisnickoIme"));
		element.sendKeys("korisnik2");
		
		element = driver.findElement(By.name("lozinka"));
		element.sendKeys("korisnik2");
		
		driver.findElement(By.cssSelector("#root > div > div > form > button")).click();
		
		 new WebDriverWait(driver, 10)
         .until(ExpectedConditions.textToBePresentInElement(driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/h2")),"Prijavljeni ste kao kartograf."));
		
		String message = driver.findElement(By.xpath("/html/body/div/div/div/h2")).getText();
		
		assertEquals("Prijavljeni ste kao kartograf.", message);
		
		driver.quit();
		
		
	}
	
	
}



