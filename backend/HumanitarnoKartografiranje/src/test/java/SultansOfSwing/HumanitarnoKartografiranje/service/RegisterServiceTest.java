package SultansOfSwing.HumanitarnoKartografiranje.service;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;


import SultansOfSwing.HumanitarnoKartografiranje.service.RegisterService;


import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;




@ExtendWith(SpringExtension.class)
@SpringBootTest
public class RegisterServiceTest {

@Autowired
RegisterService registerservice;
	

	@Test
	public void checkPasswordTest() {
	  //  RegisterService registerservice;
	    String password = "lozinka123";
	    assertDoesNotThrow( ()-> registerservice.checkPassword(password));
	}

	@Test
	public void checkRoleTest() {
	   // RegisterService registerservice = null;
	    String role = "Voditelj";
	    Exception ex = assertThrows(IllegalArgumentException.class, ()-> registerservice.checkRole(role));  
	    String message = "Role isn't valid";
	    assertEquals(message, ex.getMessage());
	}

	@Test
	public void checkUsernameTest() {
	   // RegisterService registerservice;
	    String username = "korisnik123";
	    assertDoesNotThrow( ()-> registerservice.checkUsername(username));
	}

	@Test
	public void checkUsernameTest1() {
	   // RegisterService registerservice;
	    String username = "kor";
	    Exception ex = assertThrows(IllegalArgumentException.class, ()-> registerservice.checkUsername(username));
	    String message = "Username must be between 4 and 16 characters.";
	    assertEquals(message, ex.getMessage());
	}

	@Test 
	public void checkPresenceTest() {
	   // RegisterService registerservice;
	    String entry = "Something's written";
	    assertTrue(registerservice.checkPresence(entry));
	}

	@Test 
	public void checkPresenceTest1() {
	   // RegisterService registerservice;
	    String entry = " ";
	    assertFalse(registerservice.checkPresence(entry));
	}
	
	@Test
	public void checkPasswordTest1() {
	    String password = null;
	    String message = "Password must be provided";
	    Exception ex = assertThrows(IllegalArgumentException.class, ()-> registerservice.checkPassword(password));
	    assertEquals(message, ex.getMessage());
	} 

	@Test
	public void checkPasswordTest2() {
	    String password = "passw";
	    Exception ex = assertThrows(IllegalArgumentException.class, ()-> registerservice.checkPassword(password));
	    String message = "Password must be between 6 and 32 characters.";
	    assertEquals(message, ex.getMessage());
	}

	@Test
	public void checkRoleTest1() {
	    String role = null;
	    String message = "Role must be provided";
	    Exception ex = assertThrows(IllegalArgumentException.class, ()-> registerservice.checkRole(role));
	    assertEquals(message, ex.getMessage());
	}

	@Test
	public void checkRoleTest2() {
	    String role = "voditelj";
	    assertDoesNotThrow(()-> registerservice.checkRole(role));
	}

	@Test
	public void checkUsernameTest2() {
	    String username = null;
	    String message = "Username must be provided";
	    Exception ex = assertThrows(IllegalArgumentException.class, ()-> registerservice.checkUsername(username));
	    assertEquals(message, ex.getMessage());
	}

	@Test
	public void CheckPresenceTest2() {
	    String entry = null;
	    assertFalse(registerservice.checkPresence(entry));
	}
}
